#ifndef PRELUDE_LIST_FUNCTIONS_INTERNAL_H
#define PRELUDE_LIST_FUNCTIONS_INTERNAL_H

/**
 * @file list_functions_internal.h
 * list_functions_internal headre.
 * This file is separated from list_functions.h for mempty declaration before
 * concepts which use it. I hope th next gcc version will fix it and no
 * forward declaration of mempty will be required before concepts.
 */

#include <ranges>
#include <type_traits>

namespace prelude {

  namespace monoid {

    namespace detail {
      template <typename T>
      struct empty_type{};
    } /* namespace detail */

    template <typename T>
      requires std::ranges::range<T>
            && std::is_constructible_v<T>
    [[nodiscard]] constexpr T mempty_impl(detail::empty_type<T>) {
      return T{};
    }
  } /* namespace monoid */

} /* namespace prelude */

#endif /* PRELUDE_LIST_FUNCTIONS_INTERNAL_H */
