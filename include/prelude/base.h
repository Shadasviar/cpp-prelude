#ifndef PRELUDE_BASE_H
#define PRELUDE_BASE_H

#include <utility>
#include <functional>

/**
 * @brief Partial apply given arguments to the given function.
 */
#define curry(f, ...) \
  [&](auto&& ... args) \
  -> decltype(f(__VA_ARGS__ __VA_OPT__(,) std::forward<decltype(args)>(args)...)) { \
    return f(__VA_ARGS__ __VA_OPT__(,) std::forward<decltype(args)>(args)...); \
  }

/**
 * @brief Partial apply given arguments of the class method.
 */
#define curry_class(obj, f, ...) \
  [&](auto&& ... args) \
  -> decltype((obj).f(__VA_ARGS__ __VA_OPT__(,) std::forward<decltype(args)>(args)...)) { \
    return (obj).f(__VA_ARGS__ __VA_OPT__(,) std::forward<decltype(args)>(args)...); \
  }

/**
 * @brief Make functional object from given function. This macro allow to wrap
 * primitive functions for using in overloaded operators like composition
 * by *.
 */
#define make_func_object(name, f) \
  struct name##_struct { \
    constexpr auto operator()(auto&& ... args) const { \
      return std::invoke(f, std::forward<decltype(args)>(args)...); \
    } \
  }; \
  static constexpr const name##_struct name;

#define unreachable_branch(msg) \
  []<bool flag = false>(){static_assert(flag, msg);}()

namespace prelude::base {

  template <typename F1, typename F2>
  /**
   * @brief Compose 2 functions into one.
   * compose(f,g)(x) = f(g(x)).
   */
  constexpr auto compose(F1 &&f, F2 &&g) {
    return [f=std::forward<F1>(f), g=std::forward<F2>(g)] (auto&& ... args) {
      return std::invoke
        ( f
        , std::invoke(g, std::forward<decltype(args)>(args)...)
        );
    };
  }

  template <typename F1, typename F2>
  /**
   * @brief Function composition implemented as operator.
   * (f * g)(x) = g(f(x)).
   */
  constexpr auto operator*(F1 &&f, F2 &&g) {
    return compose(std::forward<F2>(g), std::forward<F1>(f));
  }

  template <typename T1, typename T2>
  /**
   * @brief Always returns first argument.
   */
  constexpr T1 fconst(T1 &&x, T2&&) {
    return x;
  }

  /**
   * @brief Identity function.
   */
  constexpr auto id(auto &&x) {
    return x;
  }

  template
    < typename F
    , typename T1
    , typename T2 = std::invoke_result_t<F, T1>
    >
  /**
   * @brief Function application operator.
   */
  constexpr T2 operator|(T1 &&x, F &&f) {
    return std::invoke(std::forward<F>(f), std::forward<T1>(x));
  }

  /**
   * @brief Return const reference to the argument.
   */
  constexpr auto cr(auto&& x) {
    return const_cast<const std::remove_reference_t<decltype(x)>&>(x);
  }
} /* prelude::base */

#endif /* PRELUDE_BASE_H */
