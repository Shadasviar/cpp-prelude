#ifndef PRELUDE_ARRAY_H
#define PRELUDE_ARRAY_H

#include <array>
#include <algorithm>

namespace prelude {

  namespace functor {

    template
      < typename T
      , size_t N
      , typename F
      , typename T2 = std::invoke_result_t<F, T>
      >
    [[nodiscard]] constexpr std::array<T2, N> fmap_impl
      ( F &&f
      , const std::array<T, N> &a
      ) {
      std::array<T2, N> res;
      std::transform(a.begin(), a.end(), res.begin(), std::forward<F>(f));
      return res;
    }

    template
      < typename T
      , size_t N
      , typename F
      >
    constexpr std::array<T, N>& fmap_impl
      ( F &&f
      , std::array<T, N> &a
      ) {
      std::transform(a.begin(), a.end(), a.begin(), std::forward<F>(f));
      return a;
    }
  } /* functor namespace */

  namespace applicative {

    namespace detail {
      constexpr void product_part(auto &res_ptr, auto &&f, const auto& xs) {
        for (auto &x : xs) {
          *res_ptr++ = f(x);
        }
      }

      constexpr void product_part
        (auto &res_ptr, auto &&f, const auto& xs, const auto& ... xss) {
        for (auto &x : xs) {
          product_part(res_ptr, curry(f, x), xss...);
        }
      }

    } /* detail namespace */

    template
      < typename F
      , size_t N
      , typename ... Ts
      , size_t ... Ns
      , typename Res = std::invoke_result_t<F, Ts ...>
      , size_t N2 = (N * ... * Ns)
      >
    [[nodiscard]] constexpr std::array<Res, N2> lift_impl
      ( const std::array<F, N> &x
      , const std::array<Ts, Ns> & ... xs
      ) {
      std::array<Res, N2> res;
      auto res_ptr = res.begin();

      for (auto &f : x) {
        detail::product_part(res_ptr, f, xs...);
      }

      return res;
    }
  } /* applicative namespace */

  namespace semigroup {
    template
      < typename T
      , size_t N1
      , size_t N2
      , size_t N3 = N1 + N2
      >
    [[nodiscard]] constexpr std::array<T, N3> semigroup_op_impl
      ( const std::array<T, N1> &b
      , const std::array<T, N2> &a
      ) {
      std::array<T, N3> res;
      auto end = std::copy(a.begin(), a.end(), res.begin());
      std::copy(b.begin(), b.end(), end);
      return res;
    }

  } /* namespace semigroup */

  namespace detail {
    template <typename T>
    struct is_std_array : public std::false_type {};

    template <typename T, size_t N>
    struct is_std_array<std::array<T, N>> : public std::true_type{};
  } /* namespace detail */

  namespace monoid {
    template <typename T, size_t N>
    [[nodiscard]] constexpr std::array<T, 0> mempty_impl
      ( detail::empty_type<std::array<T, N>>
      ) {
      return std::array<T, 0>{};
    }
  } /* namespace monoid */

} /* namespace */

#endif /* PRELUDE_ARRAY_H */
