#ifndef PRELUDE_VECTOR_H
#define PRELUDE_VECTOR_H

#include <vector>
#include <algorithm>

#include <prelude/base.h>

namespace prelude {

  namespace functor {
    template
      < typename T1
      , typename Allocator1
      , typename F
      , typename T2 = std::invoke_result_t<F, T1>
      , typename Allocator2 = std::allocator<T2>
      >
    [[nodiscard]] constexpr std::vector<T2, Allocator2> fmap_impl
      ( F &&f
      , const std::vector<T1, Allocator1> &v
      ) {
      std::vector<T2, Allocator2> res(v.size());
      std::transform(v.begin(), v.end(), res.begin(), std::forward<F>(f));
      return res;
    }

    template
      < typename T
      , typename Allocator
      , typename F
      >
    constexpr std::vector<T, Allocator>& fmap_impl
      ( F &&f
      , std::vector<T, Allocator> &v
      ) {
      std::transform(v.begin(), v.end(), v.begin(), std::forward<F>(f));
      return v;
    }
  }

  namespace applicative {

    namespace vector::details {
      constexpr void product_part
        (auto &res_ptr, auto &&f, const auto& xs) {
        for (auto &x : xs) {
          *res_ptr++ = f(x);
        }
      }

      constexpr void product_part
        (auto &res_ptr, auto &&f, const auto& xs, const auto& ... xss) {
        for (auto &x : xs) {
          product_part(res_ptr, curry(f, x), xss...);
        }
      }
    } /* namespace vector::details */

    template
      < typename F
      , typename Allocator
      , typename ... Ts
      , typename ... As
      , typename Res = std::invoke_result_t<F, Ts...>
      , typename Allocator2 = std::allocator<Res>
      >
    [[nodiscard]] constexpr std::vector<Res, Allocator2> lift_impl
      ( const std::vector<F, Allocator> &v
      , const std::vector<Ts, As>& ... xs
      ) {
      std::vector<Res, Allocator2> res((v.size() * ... * xs.size()));
      auto res_ptr = res.begin();

      for (auto &f : v) {
        vector::details::product_part(res_ptr, f, xs...);
      }

      return res;
    }
  } /* namespace prelude::applicative */

  namespace monad {

    template
      < typename F
      , typename T
      , typename Allocator1
      , typename Res = typename std::invoke_result_t<F, T>::value_type
      , typename Allocator2 = std::allocator<Res>
      , typename AllocatorBuf = std::allocator<std::vector<Res, Allocator2>>
      >
    [[nodiscard]] constexpr std::vector<Res, Allocator2> mbind_impl
      ( F &&f
      , const std::vector<T, Allocator1> &x
      ) {
      std::vector<Res, Allocator2> res;
      auto tmp = functor::fmap_impl
        < T
        , Allocator1
        , F
        , std::vector<Res, Allocator2>
        , AllocatorBuf
        > (std::forward<F>(f), x);
      for (auto &part : tmp) {
        res.insert(res.end(), part.begin(), part.end());
      }
      return res;
    }

    template
      < typename T
      , typename Allocator
      , typename F
      , typename AllocatorBuf = std::allocator<std::vector<T, Allocator>>
      >
    constexpr std::vector<T, Allocator>& mbind_impl
      ( F &&f
      , std::vector<T, Allocator> &x
      ) {
      auto parts = functor::fmap_impl
        < T
        , Allocator
        , F
        , std::vector<T, Allocator>
        , AllocatorBuf
        > (std::forward<F>(f), base::cr(x));
      x.clear();
      for (auto &&part : std::move(parts)) {
        x.insert(x.end(), part.begin(), part.end());
      }
      return x;
    }

    template
      < typename T
      , typename Allocator1
      , typename F
      , typename Res = typename std::invoke_result_t<F, T>::value_type
      , typename Allocator2 = std::allocator<Res>
      >
    [[nodiscard]] constexpr std::vector<Res, Allocator2> mvoid_bind_impl
      ( F &&f
      , const std::vector<T, Allocator1> &
      ) {
      return std::invoke(std::forward<F>(f));
    }

    template
      < typename Res
      , typename Allocator2
      , typename T
      , typename Allocator1
      >
    [[nodiscard]] constexpr std::vector<Res, Allocator2> mvoid_bind_impl
      ( const std::vector<Res, Allocator2> &x
      , const std::vector<T, Allocator1> &
      ) {
      return x;
    }

    template
      < typename T
      , typename Allocator
      , typename F
      >
      requires std::invocable<F>
    constexpr std::vector<T, Allocator>& mvoid_bind_impl
      ( F &&f
      , std::vector<T, Allocator> &x
      ) {
      x = std::invoke(std::forward<F>(f));
      return x;
    }

    template
      < typename T
      , typename Allocator
      >
    constexpr std::vector<T, Allocator>& mvoid_bind_impl
      ( const std::vector<T, Allocator> &x
      , std::vector<T, Allocator> &res
      ) {
      res = x;
      return res;
    }

  } /* namespace monad */

  namespace semigroup {
    template <typename T, typename Allocator>
    [[nodiscard]] constexpr std::vector<T, Allocator> semigroup_op_impl
      ( const std::vector<T, Allocator> &b
      , const std::vector<T, Allocator> &a
      ) {
      std::vector<T, Allocator> res;
      res.reserve(a.size() + b.size());
      res.insert(res.end(), a.begin(), a.end());
      res.insert(res.end(), b.begin(), b.end());
      return res;
    }

    template <typename T, typename Allocator>
    constexpr std::vector<T, Allocator>& semigroup_op_impl
      ( const std::vector<T, Allocator> &b
      , std::vector<T, Allocator> &a
      ) {
      a.insert(a.end(), b.begin(), b.end());
      return a;
    }
  } /* namespace semigroup */

} /* namespace */

#endif /* PRELUDE_VECTOR_H */
