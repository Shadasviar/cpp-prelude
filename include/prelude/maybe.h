#ifndef PRELUDE_MAYBE_H
#define PRELUDE_MAYBE_H

#include <optional>

namespace prelude {

  template <typename T>
  using maybe = std::optional<T>;

  template <typename T>
  using just = maybe<T>;

  inline constexpr auto nothing = std::nullopt;

  namespace functor {

    template
      < typename F
      , typename T1
      , typename T2 = std::invoke_result_t<F, T1>
      >
    [[nodiscard]] constexpr maybe<T2> fmap_impl(F &&f, const maybe<T1> &x) noexcept {
      if (x) return just<T2>(std::invoke(std::forward<F>(f), *x));
      return nothing;
    }

    template
      < typename F
      , typename T
      > requires std::same_as<std::invoke_result_t<F, T>, T>
    constexpr maybe<T>& fmap_impl(F &&f, maybe<T> &x) noexcept {
      if (x) x = std::invoke(std::forward<F>(f), *x);
      return x;
    }

  } /* namespace functor */

  namespace applicative {

    template
      < typename F
      , typename ... Ts
      , typename T = std::invoke_result_t<F, Ts...>
      >
    [[nodiscard]] constexpr maybe<T> lift_impl
      ( const maybe<F> &f
      , const maybe<Ts>& ... xs
      ) noexcept {
      if (!(f && ... && xs)) return nothing;
      return std::invoke(*f, *xs ...);
    }

  } /* namespace applicative */

  namespace monad {

    template
      < typename F
      , typename T1
      , typename T2 = std::invoke_result_t<F, T1>
      >
    [[nodiscard]] constexpr T2 mbind_impl(F &&f, const maybe<T1> &x) noexcept {
      if (x) return std::invoke(std::forward<F>(f), *x);
      return nothing;
    }

    template
      < typename F
      , typename T
      >
    constexpr maybe<T>& mbind_impl(F &&f, maybe<T> &x) noexcept {
      if (x) x = std::invoke(std::forward<F>(f), *x);
      return x;
    }

    template
      < typename F
      , typename T1
      , typename T2 = std::invoke_result_t<F>
      >
    [[nodiscard]] constexpr T2 mvoid_bind_impl(F &&f, const maybe<T1> &x) noexcept {
      if (x) return std::invoke(std::forward<F>(f));
      return nothing;
    }

    template <typename T1, typename T2>
    [[nodiscard]] constexpr maybe<T2> mvoid_bind_impl
      ( const maybe<T2> &y
      , const maybe<T1> &x
      ) noexcept {
      if (x) return y;
      return nothing;
    }

    template
      < typename F
      , typename T
      > requires std::same_as<maybe<T>, std::invoke_result_t<F>>
    constexpr maybe<T>& mvoid_bind_impl(F &&f, maybe<T> &x) noexcept {
      if (x) x = std::invoke(std::forward<F>(f));
      return x;
    }

    template <typename T>
    constexpr maybe<T>& mvoid_bind_impl(const maybe<T> &y, maybe<T> &x) noexcept {
      if (x) x = y;
      return x;
    }

  } /* namespace monad */

  namespace foldable {

    template
      < typename F
      , std::ranges::range R
      , typename Acc
      , typename VIterator = decltype(std::begin(std::declval<R>()))
      , typename V = typename std::iterator_traits<VIterator>::value_type
      , typename T = typename V::value_type
      > requires std::same_as<V, maybe<T>>
              && std::same_as<std::invoke_result_t<F, Acc, T>, Acc>
    [[nodiscard]] constexpr maybe<Acc> fold_impl
      ( F &&f
      , Acc &&acc
      , const R &range
      ) {
      for (auto &&x : range) {
        if (x) acc = std::invoke(std::forward<F>(f), std::forward<Acc>(acc), *x);
        else return nothing;
      }
      return acc;
    }

    template <typename F, typename A, typename T>
      requires std::same_as<std::invoke_result_t<F, A, T>, A>
    [[nodiscard]] constexpr A fold_impl(F &&f, A && acc, const maybe<T> &x) {
      if (!x) return acc;
      return std::invoke(std::forward<F>(f), std::forward<A>(acc), *x);
    }

  } /* namespace foldable */

  namespace semigroup {
    template <typename T>
    [[nodiscard]] constexpr maybe<T> semigroup_op_impl
      ( const maybe<T> &b
      , const maybe<T> &a
      ) {
      if (!b) return a;
      if (!a) return b;
      return semigroup_op_impl(*b, *a);
    }

    template <typename T>
    constexpr maybe<T>& semigroup_op_impl
      ( const maybe<T> &b
      , maybe<T> &a
      ) {
      if (!b) return a;
      if (!a) {
        a = b;
        return a;
      }
      *a = semigroup_op_impl(*b, *a);
      return a;
    }
  } /* namespace semigroup */

  namespace monoid {
    template <typename T>
    [[nodiscard]] constexpr maybe<T> mempty_impl
      ( detail::empty_type<maybe<T>>
      ) {
      return nothing;
    }
  } /* namespace monoid */

} /* prelude namespace */

#endif /* PRELUDE_MAYBE_H */
