#ifndef PRELUDE_FUNCTIONAL_H
#define PRELUDE_FUNCTIONAL_H

#include <prelude/list_functions_internal.h>
#include <prelude/base.h>
#include <prelude/vector.h>
#include <prelude/array.h>
#include <prelude/maybe.h>
#include <prelude/either.h>
#include <prelude/list_functions.h>

namespace prelude {

  namespace functor {
    /**
     * @brief Functor construction function, which gets function to be applied
     * and returns functor object, which can be passed to pipeline.
     * Your must to implement fmap_impl function as free function or as your
     * functor instance member, which can be invoked with your type as first
     * parameter and function as second one.
     * You fmap_impl function must satisfy type:
     * fmap_impl :: (a -> b) -> F a -> F b
     * , where F is your functor instance type, and a/b are types instanting
     * your functor. This means, that functor must not to change functor
     * internal structure.
     */
    [[nodiscard]] inline constexpr auto map(auto &&f) {
      return curry(fmap_impl, std::forward<decltype(f)>(f));
    }
  } /* functor namespace */

  namespace applicative {
    /**
     * @brief Apply applicative functor to list of functors as arguments.
     * This function must be called only on applicatives, which stores
     * function, not value.
     *
     * It requires from user type to implement lift_impl function, which applies
     * functor function to appropriate number of functors.
     * The lift_impl function must be template and must to have a type like below:
     *
     * @code{.cpp}
     *  template
     *   < typename T
     *   , typename ... Ts
     *   , typename Res = std::invoke_result_t<T, Ts...>
     *   >
     * constexpr T<Res> lift_impl(const A<T> &x, const A<Ts>& ... pf) const {
     * @endcode
     */
    [[nodiscard]] inline constexpr auto lift(auto&& ...args) {
      return [&](auto &&f) {
        return lift_impl
          ( std::forward<decltype(f)>(f)
          , std::forward<decltype(args)>(args)...
          );}
      ;
    }
  } /* applicative namespace */

  namespace monad {
    /**
     * @brief Monad binding function.
     * Your class must implement mbind_impl function
     * which must have a type:
     * mbind :: M a -> (a -> M b) -> M b
     *  where M is your monad implementation, and a,b are types which
     *  instatniate your monad.
     */
    [[nodiscard]] inline constexpr auto bind(auto &&f) {
      return curry(mbind_impl, std::forward<decltype(f)>(f));
    }

    /**
     * @brief Void monad binding function. Your class must implement function
     * mvoid_bind_impl :: M a -> M b -> M b
     *  where M is your monad type, and a,b are types instatniate your monad.
     * You can implement mvoid_bind_impl overloads for function and for object,
     * so void binding will be able for both situations.
     */
    [[nodiscard]] inline constexpr auto void_bind(auto &&m) {
      return curry(mvoid_bind_impl, std::forward<decltype(m)>(m));
    }
  } /* monad namespace */

  namespace foldable {
    /**
     * @brief Fold class by given accumulating function with given initial value.
     * @param f Accumulating function to be used. It must have a type a -> b -> a
     * where a is accumulator type, and b is folded type.
     * @param acc Initial accumulator value.
     */
    [[nodiscard]] inline constexpr auto fold(auto &&f, auto &&acc) {
      return curry
        ( fold_impl
        , std::forward<decltype(f)>(f)
        , std::forward<decltype(acc)>(acc)
        );
    }

    [[nodiscard]] inline constexpr auto fold_m(auto &&f, auto &&acc) {
      return curry
        ( fold_m_impl
        , std::forward<decltype(f)>(f)
        , std::forward<decltype(acc)>(acc)
        );
    }
  } /* fold namespace */

  namespace semigroup {
    /**
     * @brief Semigroup operation application. The semigroup_op_impl must be
     * associative, it means that
     * (a | semigroup_op_impl(b)) | semigroup_op_impl(c) is equal to
     * a | semigroup_op_impl(b | semigroup_op_impl(c))
     */
    template <typename T>
    [[nodiscard]] inline constexpr auto semigroup_op(T &&x) {
      return curry(semigroup_op_impl, std::forward<T>(x));
    }
  } /* namespace semigroup */

  namespace monoid {
    /**
     * @brief Monoid operation application. The semigroup_op_impl must be
     * associative. See semigroup for more details.
     */
    [[nodiscard]] inline constexpr auto mappend(auto &&x) {
      using namespace semigroup;
      return curry(semigroup_op_impl, std::forward<decltype(x)>(x));
    }

    template <typename T>
    [[nodiscard]] inline constexpr auto mempty() {
      return mempty_impl(detail::empty_type<std::decay_t<T>>{});
    }
  } /* namespace monoid */

  using namespace base;
  using namespace functor;
  using namespace applicative;
  using namespace monad;
  using namespace foldable;
  using namespace semigroup;
  using namespace monoid;

} /* namespace prelude */

#endif /* PRELUDE_FUNCTIONAL_H */
