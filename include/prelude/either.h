#ifndef PRELUDE_EITHER_H
#define PRELUDE_EITHER_H

#include <variant>

#include <prelude/base.h>

namespace prelude {

  template <typename T>
  struct left {
    constexpr auto operator<=>(const left<T>&) const = default;
    constexpr bool operator==(const left<T>&) const = default;
    constexpr explicit left(const T &x) : value(x) {};

    T value;
    using value_type = T;
  };

  template <typename T>
  struct right {
    constexpr auto operator<=>(const right<T>&) const = default;
    constexpr bool operator==(const right<T>&) const = default;
    constexpr explicit right(const T &x) : value(x) {};

    T value;
    using value_type = T;
  };

  template <typename L, typename R>
  using either = std::variant<left<L>, right<R>>;

  namespace detail {

    template
      < typename Either
      , typename L = decltype(std::get<0>(std::declval<Either>()))
      , typename R = decltype(std::get<1>(std::declval<Either>()))
      >
    struct either_traits {
      using left_value_type = typename std::decay_t<L>::value_type;
      using right_value_type = typename std::decay_t<R>::value_type;
    };

  } /* namespace detail */

  template
    < typename FL
    , typename FR
    , typename L
    , typename R1
    , typename R2 = std::invoke_result_t<FR, R1>
    > requires std::same_as<std::invoke_result_t<FL, L>, R2>
  [[nodiscard]] constexpr R2 either_visit
    ( FL &&fl
    , FR &&fr
    , const either<L, R1> &x
    ) noexcept {
    if (auto *r = std::get_if<right<R1>>(&x))
      return std::invoke(std::forward<FR>(fr), r->value);
    else {
      auto *l = std::get_if<left<L>>(&x);
      return std::invoke(std::forward<FL>(fl), l->value);
    }
  }

  template
    < typename F
    , typename L
    , typename R1
    , typename R2 = std::invoke_result_t<F, R1>
    >
  [[nodiscard]] constexpr either<L, R2> map_right
    ( F &&f
    , const either<L, R1> &x
    ) noexcept {
    if (auto r = std::get_if<right<R1>>(&x))
      return right<R2>{std::invoke(std::forward<F>(f), r->value)};
    return *std::get_if<left<L>>(&x);
  }

  template <typename F, typename L, typename R>
  either<L, R>& map_right(F &&f, either<L, R> &x) noexcept {
    if (auto r = std::get_if<right<R>>(&x))
      r->value = std::invoke(std::forward<F>(f), r->value);
    return x;
  }

  template
    < typename F
    , typename L1
    , typename R
    , typename L2 = std::invoke_result_t<F, L1>
    >
  [[nodiscard]] constexpr either<L2, R> map_left
    ( F &&f
    , const either<L1, R> &x
    ) noexcept {
    if (auto l = std::get_if<left<L1>>(&x))
      return left<L2>{std::invoke(std::forward<F>(f), l->value)};
    return *std::get_if<right<R>>(&x);
  }

  template <typename F, typename L, typename R>
  constexpr either<L, R>& map_left(F &&f, either<L, R> &x) noexcept {
    if (auto l = std::get_if<left<L>>(&x))
      l->value = std::invoke(std::forward<F>(f), l->value);
    return x;
  }

  namespace functor {

    template
      < typename F
      , typename L
      , typename R1
      , typename R2 = std::invoke_result_t<F, R1>
      >
    [[nodiscard]] constexpr either<L, R2> fmap_impl
      ( F &&f
      , const either<L, R1> &x
      ) noexcept {
      return map_right(std::forward<F>(f), x);
    }

    template <typename F, typename L, typename R>
    constexpr either<L, R>& fmap_impl(F &&f, either<L, R> &x) noexcept {
      return map_right(std::forward<F>(f), x);
    }

  } /* namespace functor */

  namespace applicative {

    template
      < typename F
      , typename L
      , typename R1
      , typename R2 = std::invoke_result_t<F, R1>
      >
    [[nodiscard]] constexpr either<L, R2> lift_impl
      ( const either<L, F> &f
      , const either<L, R1> &x
      ) noexcept {
      if (auto r = std::get_if<right<F>>(&f))
        return map_right(r->value, x);
      return *std::get_if<left<L>>(&f);
    }

    template
      < typename F
      , typename L
      , typename R1
      , typename ... Rs
      , typename Res = std::invoke_result_t<F, R1, Rs...>
      >
    [[nodiscard]] constexpr either<L, Res> lift_impl
      ( const either<L, F> &either_f
      , const either<L, R1> &x
      , const either<L, Rs>& ... xs
      ) noexcept {
      auto forward_left = [] (auto l) {return either<L, Res>{left{l}};};

      return either_visit
        ( forward_left
        , [&] (auto f) {
          return either_visit
            ( forward_left
            , [&] (auto r) {
              auto f2 = curry(f, r);
              return lift_impl(either<L, decltype(f2)>{right{f2}}, xs...);
            }, x
            );
        }, either_f
        );
    }

  } /* namespace applicative */

  namespace monad {

    template
      < typename F
      , typename L
      , typename R
      , typename Res = std::invoke_result_t<F, R>
      >
    [[nodiscard]] constexpr Res mbind_impl
      ( F &&f
      , const either<L, R> &x
      ) noexcept {
      if (auto r = std::get_if<right<R>>(&x))
        return std::invoke(std::forward<F>(f), r->value);
      return *std::get_if<left<L>>(&x);
    }

    template <typename F, typename L, typename R>
    either<L, R>& mbind_impl(F &&f, either<L, R> &x) noexcept {
      if (auto r = std::get_if<right<R>>(&x))
        x = std::invoke(std::forward<F>(f), r->value);
      return x;
    }

    template
      < typename F
      , typename L
      , typename R
      , typename Res = std::invoke_result_t<F>
      >
    [[nodiscard]] constexpr Res mvoid_bind_impl
      ( F &&f
      , const either<L, R> &x
      ) noexcept {
      if (auto r = std::get_if<right<R>>(&x))
        return std::invoke(std::forward<F>(f));
      return *std::get_if<left<L>>(&x);
    }

    template <typename L, typename R, typename Res>
    [[nodiscard]] constexpr either<L, Res> mvoid_bind_impl
      ( const either<L, Res> &res
      , const either<L, R> &x
      ) noexcept {
      if (auto r = std::get_if<right<R>>(&x))
        return res;
      return *std::get_if<left<L>>(&x);
    }

    template
      < typename F
      , typename L
      , typename R
      > requires std::same_as<either<L, R>, std::invoke_result_t<F>>
    constexpr either<L, R>& mvoid_bind_impl(F &&f, either<L, R> &x) noexcept {
      if (auto r = std::get_if<right<R>>(&x))
        x = std::invoke(std::forward<F>(f));
      return x;
    }

    template <typename L, typename R>
    constexpr either<L, R>& mvoid_bind_impl
      ( const either<L, R> &res
      , either<L, R> &x
      ) noexcept {
      if (auto r = std::get_if<right<R>>(&x))
        x = res;
      return x;
    }

  } /* namespace monad */

  namespace foldable {

    template
      < typename F
      , std::ranges::range Range
      , typename Acc
      , typename VIterator = decltype(std::begin(std::declval<Range>()))
      , typename V = typename std::iterator_traits<VIterator>::value_type
      , typename E = typename detail::either_traits<V>
      , typename R = typename E::right_value_type
      , typename L = typename E::left_value_type
      > requires std::same_as<V, either<L, R>>
              && std::same_as<std::invoke_result_t<F, Acc, R>, Acc>
    [[nodiscard]] constexpr either<L, Acc> fold_impl
      ( F &&f
      , Acc &&acc
      , const Range &range
      ) {
      for (auto &&x : range) {
        if (auto r = std::get_if<right<R>>(&x))
          acc = std::invoke(std::forward<F>(f), std::forward<Acc>(acc), r->value);
        if (auto l = std::get_if<left<L>>(&x)) return *l;
      }
      return right{acc};
    }

  } /* namespace foldable */

  namespace semigroup {
    template <typename L, typename R>
    [[nodiscard]] constexpr either<L, R> semigroup_op_impl
      ( const either<L, R> &b
      , const either<L, R> &a
      ) noexcept {
      auto ar = std::get_if<right<R>>(&a);
      auto br = std::get_if<right<R>>(&b);
      if (ar && br) return right{semigroup_op_impl(br->value, ar->value)};
      if (!ar) return b;
      return a;
    }

    template <typename L, typename R>
    constexpr either<L, R>& semigroup_op_impl
      ( const either<L, R> &b
      , either<L, R> &a
      ) noexcept {
      auto ar = std::get_if<right<R>>(&a);
      auto br = std::get_if<right<R>>(&b);
      if (ar && br) ar->value = semigroup_op_impl(br->value, ar->value);
      else if (!ar) a = b;
      return a;
    }
  } /* namespace semigroup */

} /* namespace */

#endif /* PRELUDE_EITHER_H */
