#ifndef PRELUDE_LIST_FUNCTIONS_H
#define PRELUDE_LIST_FUNCTIONS_H

#include <prelude/concepts.h>

#include <ranges>
#include <type_traits>
#include <numeric>
#include <iterator>

namespace prelude {
  namespace foldable {

    template <typename F, typename T, size_t N, typename Res>
    [[nodiscard]] constexpr std::decay_t<Res> fold_impl
      ( F &&f
      , Res &&acc
      , const T(&arr)[N]
      ) {
      return std::accumulate
        ( arr
        , arr + N
        , std::forward<Res>(acc)
        , std::forward<F>(f)
        );
    }

    template
      < typename F
      , std::ranges::range R
      , typename Res
      , typename VIterator = decltype(std::begin(std::declval<R>()))
      , typename V = typename std::iterator_traits<VIterator>::value_type
      > requires std::same_as<std::invoke_result_t<F, Res, V>, Res>
    [[nodiscard]] constexpr std::decay_t<Res> fold_impl
      ( F &&f
      , Res &&acc
      , const R &range
      ) {
      return std::accumulate
        ( std::begin(range)
        , std::end(range)
        , std::forward<Res>(acc)
        , std::forward<F>(f)
        );
    }

    template
      < typename F
      , std::ranges::range R
      , typename Acc
      , typename VIterator = decltype(std::begin(std::declval<R>()))
      , typename V = typename std::iterator_traits<VIterator>::value_type
      , typename MRes = std::invoke_result_t<F, Acc, V>
      , typename MF = std::function<MRes(V)>
      > requires monad::is_monad<MRes, MF, Acc>
    [[nodiscard]] constexpr MRes fold_m_impl
      ( F &&f
      , Acc &&acc
      , const R &range
      ) {
      using namespace std::placeholders;

      const auto fold_f = [&f] (MRes &&a, auto &&x) constexpr {
        using namespace monad;
        return mbind_impl(std::bind(std::forward<F>(f), _1, x), a);
      };

      return std::accumulate
        ( std::begin(range)
        , std::end(range)
        , MRes{acc}
        , fold_f
        );
    }

  } /* namespace foldable */

  namespace semigroup {
    template <std::ranges::range R, typename Res>
      requires std::is_constructible_v<Res>
            && prelude::detail::concatable<Res>
    [[nodiscard]] constexpr Res semigroup_op_impl
      ( const R &a
      , const R &b
      ) {
      Res res;
      res.insert(std::end(res), std::begin(a), std::end(a));
      res.insert(std::end(res), std::begin(b), std::end(b));
      return res;
    }
  } /* namespace semigroup */

  template
    < typename R
    , typename F
    > requires monoid::is_monoid<R>
    [[nodiscard]] constexpr R filter (F &&f, const R &xs) {
      using namespace prelude;
      using namespace semigroup;
      using namespace monoid;
      using namespace foldable;

      const auto filter_f = [f = std::forward<F>(f)] (R a, auto b) constexpr {
        if (std::invoke(f, b)) return semigroup_op_impl(R{b}, a);
        return a;
      };

      return fold_impl
        ( filter_f
        , mempty_impl(monoid::detail::empty_type<std::decay_t<R>>{})
        , xs
        );
    }

} /* namespace prelude */

#endif /* PRELUDE_LIST_FUNCTIONS_H */
