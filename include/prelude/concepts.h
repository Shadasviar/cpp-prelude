#ifndef PRELUDE_CONCEPTS_H
#define PRELUDE_CONCEPTS_H

#include <concepts>
#include <utility>

namespace prelude {

  namespace functor {
    template <typename T, typename F, typename ... Ts>
    concept is_functor = requires(T x, F f) {
      fmap_impl(f, x);
      std::is_constructible_v<T, Ts...>;
    };
  }

  namespace applicative {
    template <typename F, typename ... Ts>
    concept is_applicative = requires(F f, Ts ... xs) {
      lift_impl(f, xs ...);
    };
  }

  namespace monad {
    template <typename T, typename F, typename ... Ts>
    concept is_monad = requires(T x, F f) {
      mbind_impl(f, x);
      std::is_constructible_v<T, Ts...>;
    };

    template <typename T, typename F, typename ... Ts>
    concept is_void_monad = requires(T x, F f) {
      mvoid_bind_impl(f, x);
      std::is_constructible_v<T, Ts...>;
    };
  }

  namespace foldable {
    template <typename T, typename F, typename Acc>
    concept is_foldable = requires(T x, F f, Acc a) {
      { fold_impl(f, a, x) } -> std::same_as<Acc>;
    };

    template <typename T, typename F, typename Acc>
    concept is_m_foldable = requires(T x, F f, Acc a) {
      { fold_m_impl(f, a, x) } -> std::same_as<Acc>;
    };
  }

  namespace semigroup {
    template <typename T>
    concept is_semigroup = requires(T a, T b) {
      semigroup_op_impl(a, b);
    };
  }

  namespace monoid {
    template <typename T>
    concept is_monoid = requires(T a) {
      requires semigroup::is_semigroup<T>;
      { mempty_impl(detail::empty_type<T>{}) } -> std::same_as<T>;
    };
  }

  namespace detail {
    template <typename T>
    concept concatable = requires(T a, T b) {
      requires std::ranges::range<T>;
      a.insert(std::end(a), std::begin(b), std::end(b));
    };
  } /* namespace detail */

} /* namespace prelude */

#endif /* PRELUDE_CONCEPTS_H */
