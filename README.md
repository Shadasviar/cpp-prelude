# cpp-prelude

C++20 header-only library, which provides functional programming functionalities
inspired by Haskell prelude.

# Table of contents
- [How to use](#how-to-use)
- [Available modules](#available-modules)
   * [base](#base)
      + [curry](#curry)
      + [make_func_object](#make_func_object)
      + [functional composition](#functional-composition)
      + [identity function](#identity-function)
      + [pipe operator](#pipe-operator)
      + [add const reference](#add-const-reference)
   * [functor](#functor)
   * [applicative](#applicative)
   * [semigroup](#semigroup)
   * [monoid](#monoid)
   * [monad](#monad)
   * [foldable](#foldable)

# How to use

`cpp-prelude` library is header-only, so you can just add `include`
directory of this repository to your project include path, and library will
be ready to use.

This library requires `C++20` standard enabled.

For use `prelude` library you need only include `prelude/functional.h` header
file. You may often need to add `using namespace prelude` directive to make
overloaded operators available in your code.

# Available modules

This section describes available library function and interfaces.
This library allows user implement custom behaviour for custom types and even
for standard library structures.

Actually `cpp-prelude` provides modules described below.

## base

The base module provides some basic functional features, which are widely
used in functional programming.

This header file contains some useful macros:

### curry

This macro in pair with `curry_class` allow to partial apply function
to given arguments:

```c++
struct S {
  constexpr float div(float x, float z) const {
    return x / y + z;
  }

  float y = 2.0f;
};

constexpr float add(float x, float y) {
  return x + y;
}

int main () {
  S s{5.0f};

  auto f = curry(add, 5);
  auto z = f(2); // z = 7;
  auto x = curry_class(s, div, 10)(2); // c = 4;
}
```

### make_func_object

This macro makes functional object from given function. It can be useful
in situations where compiler doesn't allow to pass usual function references.
`make_func_object` defines structure with operator `()`, which calls given
by user function. Maco also creats this structure static object with given name.

```c++
constexpr float f(int x) {
  return x / 2.0;
}

make_func_object(F, f)

int main () {
  F(5) == f(5);
}
```

## functional composition

In `prelude` library, function composition is implemented as `operator*` for
two functions. It works like `(f * g)(x) == g(f(x))`.

## identity function

Identity function is available in `base` module by name `id`. It works
like `id(x) == x;`.

## pipe operator

The pipe operator is inspired by unix pipes and corresponding C++20 operator
used in ranges library. It works like `(x | f) == f(x);`.

This library is built around composing and pipelining functions like `map`,
`lift`, `fold` etc.

## add const reference

If you build some overloads of your functions for const and in-place variants
of your types, you probably will need an short way to say compiler that your
object must be used as const reference instead of in-place. Most functions of
this library have in-place versions, which modify given container instead of
create new one.

For force const reference overloads of functions, `prelude` provides `cr`
function, which just returns const reference to the gicen object:
```c++
int x = 5;
auto y = cr(x); // y is const int&
```

## functor

Functor allow user to apply function on the object without changing object
structure.

Functor provides `map` function, which has a signature:
```c++
constexpr auto map(auto &&f);
```

`map` function gets function `f` and returns function, which applies `f`
to the target object using `fmap_impl` function. `fmap_impl` should be
implemented by user for custom types for support prelude functor interface.

`fmap_impl` function implementation example for custom type is presented below:
```c++
#inlude <prelude/functional.h>

/* Custom class which needs to be functor */
template <typename T>
struct Point {
  T x, y;

  /* fmap_impl function implementation */
  template
    < typename F
    , typename T2 = std::invoke_result_t<F, T>
    >
  friend constexpr Point<T2> fmap_impl(F &&f, const Point<T> &p) {
    return Point<T2>{.x = f(p.x), .y = f(p.y)};
  }
};

/* And usage of the functor */
static constexpr int mul2(int x) {return x*2;}

int main() {
  using namespace prelude;

  constexpr auto a = Point<int> {.x = 5, .y = 4};
  auto b = a | map(mul2) | map(mul2);

  return 0;
}
```

## applicative

Applicative functors are represented by only one `lift` function with variadic
arguments, so it can lift any function with any arguments number into functor.

For support applicative functor instance of your types, you must to implement
`lift_impl` function. This function example for custom type is listed below:
```c++
#include <prelude/functional.h>

/* Custom Point type */
template <typename T>
struct Point {
  T x, y;
};


/* Implementation of the lift_impl for Point */
template
  < typename ... Ts
  , typename T
  , typename Res = std::invoke_result_t<T, Ts...>
  >
constexpr Point<Res> lift_impl(const Point<T> &p, const Point<Ts>& ... pf) {
  return Point<Res>{.x = p.x((pf.x)...), .y = p.y((pf.y)...)};
}

static constexpr int mul2(int x){return x*2;}
static constexpr double fx(int x, int y, double z) {return (x + y) / z;}
static constexpr double fy(int x, int y, double z) {return (x * y) / z;}

int main () {
  /* Usage of applicative functor */
  constexpr Point<int> a{2,3};
  constexpr Point<decltype(mul2)*> f1{mul2, mul2}
  constexpr auto d = f1 | lift(a); // d == Point{4,6}

  constexpr Point<decltype(fx)*> f2{fx, fy};
  constexpr Point<int> b{1, 2};
  constexpr Point<double> c{1.5, 2};
  constexpr auto g = f2 | lift(a, b, c); // g == Point{2,3}

  return 0;
}
```

## semigroup

Semigroup is any data type which have an associative operation. For implement
semigroup interface in `prelude` library, you should implement
`semigroup_op_impl` function for your type:

```c++
struct sum_int {
  int value;
  constexpr bool operator==(const sum_int&) const = default;
};

constexpr sum_int semigroup_op_impl(sum_int a, sum_int b) {
  return sum_int{a.value + b.value};
}

int main() {
  constexpr std::array<int, 3> a{1,2,3};
  constexpr std::array<int, 4> b{4,5,6,7};
  constexpr std::array<int, 7> res1{1,2,3,4,5,6,7};

  static_assert((a | semigroup_op(b)) == res1);

  return 0;
}
```

## monoid

Monoid is a semigroup which has neutral element. For implementing monoid you
should implement `semigroup` interface and `mempty_impl` function which
accepts special empty type template used for overloading function:

```c++
struct sum_int {
  int value;
  constexpr bool operator==(const sum_int&) const = default;
};

constexpr sum_int semigroup_op_impl(sum_int a, sum_int b) {
  return sum_int{a.value + b.value};
}

constexpr sum_int mempty() {
  return sum_int{0};
}

int main() {
  constexpr std::array<int, 3> a{1,2,3};
  constexpr std::array<int, 4> b{4,5,6,7};
  constexpr std::array<int, 7> res1{1,2,3,4,5,6,7};

  static_assert((a | mappend(b) | mappend(mempty<decltype(a)>())) == res1);

  return 0;
}
```

## monad

Monads are abstraction for chaining functions wich return values with some
effects. In the `prelude` library, monads interface is implemented by two
functions: `bind` and `void_bind`.

Monad binding is chaining functions, which get some normal value and return
result with some effect with arguments wich contains effects.

Void binding is chaining functions or values, which ignores value of
previous step, but checks only its state.

For implement theese interfaces, your types must to implement functions
`mbind_impl` and `mvoid_bind_impl`. See examples below:

```c++
template <typename T>
/* Class to be monad */
struct Point {
  T x, y;
  using value_t = T;

  template
    < typename F
    , typename Res = std::invoke_result_t<F, T>::value_t
    >
  /* Monad binding implementation */
  constexpr friend Point<Res> mbind_impl(F &&f, const Point &p) {
    auto res_x = f(p.x);
    auto res_y = f(p.y);
    return Point<Res>{.x = res_x.x + res_y.x, .y = res_x.y + res_y.y};
  }

  template
    < typename F
    , typename T2 = std::invoke_result_t<F>
    >
  /* Empty monad binding for function without arguments */
  constexpr friend T2 mvoid_bind_impl(F &&f, const Point&) {
    return std::invoke(std::forward<F>(f));
  }

  template <typename T2>
  /* Empty monad binding for another point */
  constexpr friend Point<T2> mvoid_bind_impl(const Point<T2> &x, const Point&) {
    return x;
  }

  constexpr bool operator==(const Point<T> &) const = default;

};

template <typename T>
/* Function for monad binding: gets value and returns result with some effect */
static constexpr Point<T> generatePoint(T x){return Point<T>{.x = x, .y = x};}

/* Functon for void binding: just returns some effect */
static constexpr Point<double> gen(){return Point<double>{.x = 1.5, .y = 2.5};}

make_func_object(generatePoint_f, generatePoint<int>)
make_func_object(generatePointd_f, generatePoint<double>)
make_func_object(gen_f, gen)

/* usage example */
int main () {
  using namespace prelude;

  constexpr auto a = generatePoint_f(5);
  constexpr auto b = generatePoint_f(2);
  constexpr Point<double> c{.x = 1.5, .y = 2};

  constexpr auto d = a | bind(generatePoint_f);
  constexpr auto e
    = b
    | bind(generatePoint_f)
    | bind(generatePoint_f)
    | bind(generatePoint_f)
    ;
  constexpr auto g = a | void_bind(b) | void_bind(c);
  constexpr auto h = c | bind(generatePoint_f) | bind(generatePointd_f);
  constexpr auto i = a | bind(generatePoint_f) | void_bind(gen_f);

  static_assert(d == Point<int>{.x = 10, .y = 10});
  static_assert(e == Point<int>{.x = 16, .y = 16});
  static_assert(g == Point<double>{.x = 1.5, .y = 2});
  static_assert(h == Point<double>{.x = 6, .y = 6});
  static_assert(i == Point<double>{.x = 1.5, .y = 2.5});

  return 0;
}
```

## foldable

Foldable interface allows to fold some structure into one value. For implement
fold interface, you need to implement `fold_impl` function for your type.

Fold function gets function which gets accumulator and your element, and
returns new accumulator value. The seconf argument of the `fold` function is
initial accumulator value.

Examples of usage folds:
```c++
template <typename T>
/* Custom foldable stucture */
struct Point {
  T x, y;

  template
    < typename F
    , typename Res
    > requires std::same_as<std::invoke_result_t<F, Res, T>, std::decay_t<Res>>
  /* Implementation of fold interface for Point */
  friend constexpr std::decay_t<Res> fold_impl
    ( F &&f
    , Res &&acc
    , const Point<T> &p
    ) {
    return f(f(std::forward<Res>(acc), p.x), p.y);
  }

  constexpr bool operator==(const Point<T>&) const = default;
  constexpr auto operator<=>(const Point<T>&) const = default;
};

constexpr prelude::maybe<double> div_x(double acc, int x) {
  using namespace prelude;

  if (x == 0) return nothing;
  return just{acc / x};
}

constexpr double sum(double acc, int x) {
  return acc + x;
}

int main() {
  using namespace prelude;

  /* Folding Point */
  constexpr Point<int> a{1,2};
  constexpr Point<int> b{3,7};

  static_assert((a | fold(sum, 0.5)) == 3.5);
  static_assert((b | fold(sum, 0.0)) == 10.0);

  /* Folding array */
  constexpr std::array<int, 5> aa{1,2,3,4,5};
  constexpr std::array<int, 3> ba{2,4,6};

  static_assert((aa | fold(sum, 0.5)) == 15.5);
  static_assert((aa | fold(sum, 2.6)) == 17.6);
  static_assert((ba | fold(sum, 1.0)) == 13.0);
  static_assert((ba | fold(sum, 7.0)) == 19.0);

  /* Folding vector */
  const std::vector<int> av{1,2,3,4,5};
  const std::vector<int> bv{2,4,6};

  (av | fold(sum, 0.0)) == 15.0;
  (av | fold(sum, 0.5)) == 15.5;

  /* Monadic folding by maybe function  */
  constexpr std::array<int, 3> am{1,2,2};
  constexpr std::array<int, 4> bm{1,0,3,4};

  static_assert((am | fold(div_x, 1.0)) == just{0.25});
  static_assert((bm | fold(div_x, 1.0)) == nothing);

  /* Folding the list of maybes */
  constexpr std::array<maybe<int>, 3> aml{just{1}, just{2}, just{2}};
  constexpr std::array<maybe<int>, 4> bml{just{1}, nothing, just{3}, just{4}};

  static_assert((aml | fold(sum, 1.0)) == just{6.0});
  static_assert((aml | fold(sum, 2.5)) == just{7.5});
  static_assert((bml | fold(sum, 1.0)) == nothing);
  static_assert((bml | fold(sum, 2.0)) == nothing);

  /* Folding list of eithers */
  using either_int = either<const char*, int>;
  using either_double = either<const char*, double>;

  constexpr std::array<either_int, 3> ae{right{1}, right{2}, right{2}};

  static_assert((ae | fold(sum, 1.0)) == either_double{right{6.0}});

  return 0;
}
```
