# Find version from git describe command in format v.major.minor-patch-hash
# Your git tags must have format of v.major.minor

SET(GIT_REPO_PATH
  "${CMAKE_CURRENT_SOURCE_DIR}/.git"
  CACHE FILEPATH
  "Path of the .git file of the repository"
)

EXECUTE_PROCESS(
  COMMAND git --git-dir "${GIT_REPO_PATH}" describe
  OUTPUT_VARIABLE GIT_OUTPUT
  OUTPUT_STRIP_TRAILING_WHITESPACE
)

STRING(
  REGEX
  MATCH "([0-9]+)\.?([0-9]+)?[\.\-]?([0-9]+)?[\.\-](.*)"
  GIT_MATCHED
  "${GIT_OUTPUT}"
)

SET(GIT_VERSION_MAJOR "${CMAKE_MATCH_1}")
SET(GIT_VERSION_MINOR "${CMAKE_MATCH_2}")
SET(GIT_VERSION_PATCH "${CMAKE_MATCH_3}")
SET(GIT_VERSION_FULL
  "${GIT_VERSION_MAJOR}.${GIT_VERSION_MINOR}.${GIT_VERSION_PATCH}"
)
SET(GIT_VERSION_HASH "${CMAKE_MATCH_4}")

