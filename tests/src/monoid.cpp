#include <prelude/functional.h>

#include <gtest/gtest.h>
#include <gtest_helpers.h>

struct sum_int {
  int value;
  constexpr bool operator==(const sum_int&) const = default;
};

constexpr sum_int semigroup_op_impl(sum_int a, sum_int b) {
  return sum_int{a.value + b.value};
}

constexpr sum_int mempty() {
  return sum_int{0};
}

enum class Error {
  ERROR_1,
  ERROR_2,
};

TEST(Monoid, Vector) {
  using namespace prelude;

  const std::vector<int> a{1,2,3};
  const std::vector<int> b{4,5,6,7};
  const std::vector<int> res1{1,2,3,4,5,6,7};
  const std::vector<int> res2{4,5,6,7,1,2,3};

  EXPECT_VECTORS_EQ((a | mappend(b)), res1);
  EXPECT_VECTORS_EQ((a | mappend(mempty<decltype(a)>())), a);
  EXPECT_VECTORS_EQ((b | mappend(a) | mappend(mempty<decltype(a)>())), res2);
}

TEST(Monoid, VectorInplace) {
  using namespace prelude;

  std::vector<int> a{1,2,3};
  const std::vector<int> b{4,5,6,7};
  const std::vector<int> res1{1,2,3,4,5,6,7};
  const std::vector<int> res2{1,2,3,4,5,6,7,1,2,3,4,5,6,7};
  const std::vector<int> res3{1,2,3,4,5,6,7,1,2,3,4,5,6,7,4,5,6,7};

  a | mappend(b) | mappend(mempty<decltype(a)>());
  EXPECT_VECTORS_EQ(a, res1);

  a | mappend(mempty<decltype(a)>()) | mappend(a);
  EXPECT_VECTORS_EQ(a, res2);

  a | mappend(b);
  EXPECT_VECTORS_EQ(a, res3);
}

TEST(Monoid, Array) {
  using namespace prelude;

  constexpr std::array<int, 3> a{1,2,3};
  constexpr std::array<int, 4> b{4,5,6,7};
  constexpr std::array<int, 7> res1{1,2,3,4,5,6,7};
  constexpr std::array<int, 7> res2{4,5,6,7,1,2,3};
  constexpr std::array<int, 10> res3{1,2,3,4,5,6,7,1,2,3};

  static_assert((a | mappend(b) | mappend(mempty<decltype(a)>())) == res1);
  static_assert((b | mappend(mempty<decltype(a)>()) | mappend(a)) == res2);
  static_assert((a | mappend(b) | mappend(a)) == res3);

  EXPECT_EQ((a | mappend(b) | mappend(mempty<decltype(a)>())), res1);
  EXPECT_EQ((b | mappend(mempty<decltype(a)>()) | mappend(a)), res2);
  EXPECT_EQ((a | mappend(b) | mappend(a)), res3);
}

TEST(Monoid, Maybe) {
  using namespace prelude;

  constexpr maybe<sum_int> a{sum_int{5}};
  constexpr maybe<sum_int> b{sum_int{4}};
  constexpr maybe<sum_int> c;

  static_assert((a | mappend(b) | mappend(mempty<decltype(a)>()))
      == just<sum_int>{sum_int{9}});
  static_assert((b | mappend(mempty<decltype(a)>()) | mappend(a))
      == just<sum_int>{sum_int{9}});
  static_assert((b | mappend(a) | mappend(mempty<decltype(a)>()) | mappend(b))
      == just<sum_int>{sum_int{13}});
  static_assert((b | mappend(mempty<decltype(a)>()) | mappend(a | mappend(b)))
      == just<sum_int>{sum_int{13}});
  static_assert((a | mappend(mempty<decltype(a)>()) | mappend(c)) == a);
  static_assert((c | mappend(b) | mappend(mempty<decltype(a)>())) == b);
  static_assert(
    ( c
    | mappend(mempty<decltype(a)>())
    | mappend(c)
    | mappend(mempty<decltype(a)>())
    ) == c);

  EXPECT_EQ((a | mappend(b) | mappend(mempty<decltype(a)>()))
      , just<sum_int>{sum_int{9}});
  EXPECT_EQ((b | mappend(mempty<decltype(a)>()) | mappend(a))
      , just<sum_int>{sum_int{9}});
  EXPECT_EQ((b | mappend(a) | mappend(mempty<decltype(a)>()) | mappend(b))
      , just<sum_int>{sum_int{13}});
  EXPECT_EQ((b | mappend(mempty<decltype(a)>()) | mappend(a | mappend(b)))
      , just<sum_int>{sum_int{13}});
  EXPECT_EQ((a | mappend(mempty<decltype(a)>()) | mappend(c)), a);
  EXPECT_EQ((c | mappend(b) | mappend(mempty<decltype(a)>())), b);
  EXPECT_EQ(
    ( c
    | mappend(mempty<decltype(a)>())
    | mappend(c)
    | mappend(mempty<decltype(a)>())
    ), c);
}

TEST(Monoid, MaybeInplace) {
  using namespace prelude;

  maybe<sum_int> a{sum_int{5}};
  const maybe<sum_int> b{sum_int{4}};
  const maybe<sum_int> c;

  a | mappend(b) | mappend(mempty<decltype(a)>());
  EXPECT_EQ(a, just<sum_int>{sum_int{9}});

  a | mappend(mempty<decltype(a)>()) | mappend(a);
  EXPECT_EQ(a, just<sum_int>{sum_int{18}});

  a | mappend(c) | mappend(mempty<decltype(a)>());
  EXPECT_EQ(a, just<sum_int>{sum_int{18}});

  a | mappend(b) | mappend(mempty<decltype(a)>()) | mappend(b);
  EXPECT_EQ(a, just<sum_int>{sum_int{26}});
}
