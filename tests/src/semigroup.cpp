#include <prelude/functional.h>

#include <gtest/gtest.h>
#include <gtest_helpers.h>

struct sum_int {
  int value;
  constexpr bool operator==(const sum_int&) const = default;
};

constexpr sum_int semigroup_op_impl(sum_int a, sum_int b) {
  return sum_int{a.value + b.value};
}

enum class Error {
  ERROR_1,
  ERROR_2,
};

TEST(Semigroup, Vector) {
  using namespace prelude;

  const std::vector<int> a{1,2,3};
  const std::vector<int> b{4,5,6,7};
  const std::vector<int> res1{1,2,3,4,5,6,7};
  const std::vector<int> res2{4,5,6,7,1,2,3};
  const std::vector<int> res3{1,2,3,4,5,6,7,1,2,3};

  EXPECT_VECTORS_EQ((a | semigroup_op(b)), res1);
  EXPECT_VECTORS_EQ((b | semigroup_op(a)), res2);
  EXPECT_VECTORS_EQ((a | semigroup_op(b) | semigroup_op(a)), res3);
}

TEST(Semigroup, VectorInplace) {
  using namespace prelude;

  std::vector<int> a{1,2,3};
  const std::vector<int> b{4,5,6,7};
  const std::vector<int> res1{1,2,3,4,5,6,7};
  const std::vector<int> res2{1,2,3,4,5,6,7,1,2,3,4,5,6,7};
  const std::vector<int> res3{1,2,3,4,5,6,7,1,2,3,4,5,6,7,4,5,6,7};

  a | semigroup_op(b);
  EXPECT_VECTORS_EQ(a, res1);

  a | semigroup_op(a);
  EXPECT_VECTORS_EQ(a, res2);

  a | semigroup_op(b);
  EXPECT_VECTORS_EQ(a, res3);
}

TEST(Semigroup, Array) {
  using namespace prelude;

  constexpr std::array<int, 3> a{1,2,3};
  constexpr std::array<int, 4> b{4,5,6,7};
  constexpr std::array<int, 7> res1{1,2,3,4,5,6,7};
  constexpr std::array<int, 7> res2{4,5,6,7,1,2,3};
  constexpr std::array<int, 10> res3{1,2,3,4,5,6,7,1,2,3};

  static_assert((a | semigroup_op(b)) == res1);
  static_assert((b | semigroup_op(a)) == res2);
  static_assert((a | semigroup_op(b) | semigroup_op(a)) == res3);

  EXPECT_VECTORS_EQ((a | semigroup_op(b)), res1);
  EXPECT_VECTORS_EQ((b | semigroup_op(a)), res2);
  EXPECT_VECTORS_EQ((a | semigroup_op(b) | semigroup_op(a)), res3);
}

TEST(Semigroup, Maybe) {
  using namespace prelude;

  constexpr maybe<sum_int> a{sum_int{5}};
  constexpr maybe<sum_int> b{sum_int{4}};
  constexpr maybe<sum_int> c;

  static_assert((a | semigroup_op(b)) == just<sum_int>{sum_int{9}});
  static_assert((b | semigroup_op(a)) == just<sum_int>{sum_int{9}});
  static_assert((b | semigroup_op(a) | semigroup_op(b)) == just<sum_int>{sum_int{13}});
  static_assert((b | semigroup_op(a | semigroup_op(b))) == just<sum_int>{sum_int{13}});
  static_assert((a | semigroup_op(c)) == a);
  static_assert((c | semigroup_op(b)) == b);
  static_assert((c | semigroup_op(c)) == c);

  EXPECT_EQ((a | semigroup_op(b)), just<sum_int>{sum_int{9}});
  EXPECT_EQ((b | semigroup_op(a)), just<sum_int>{sum_int{9}});
  EXPECT_EQ((b | semigroup_op(a) | semigroup_op(b)), just<sum_int>{sum_int{13}});
  EXPECT_EQ((b | semigroup_op(a | semigroup_op(b))), just<sum_int>{sum_int{13}});
  EXPECT_EQ((a | semigroup_op(c)), a);
  EXPECT_EQ((c | semigroup_op(b)), b);
  EXPECT_EQ((c | semigroup_op(c)), c);
}

TEST(Semigroup, MaybeInplace) {
  using namespace prelude;

  maybe<sum_int> a{sum_int{5}};
  const maybe<sum_int> b{sum_int{4}};
  const maybe<sum_int> c;

  a | semigroup_op(b);
  EXPECT_EQ(a, just<sum_int>{sum_int{9}});

  a | semigroup_op(a);
  EXPECT_EQ(a, just<sum_int>{sum_int{18}});

  a | semigroup_op(c);
  EXPECT_EQ(a, just<sum_int>{sum_int{18}});

  a | semigroup_op(b) | semigroup_op(b);
  EXPECT_EQ(a, just<sum_int>{sum_int{26}});
}

TEST(Semigroup, Either) {
  using namespace prelude;
  using E = either<Error, sum_int>;

  constexpr E a{right{sum_int{5}}};
  constexpr E b{right{sum_int{2}}};
  constexpr E c{left{Error::ERROR_1}};

  static_assert((a | semigroup_op(b)) == E{right{sum_int{7}}});
  static_assert(((a | semigroup_op(b)) | semigroup_op(a)) == E{right{sum_int{12}}});
  static_assert((a | semigroup_op(b | semigroup_op(a))) == E{right{sum_int{12}}});
  static_assert((a | semigroup_op(c)) == a);
  static_assert((c | semigroup_op(b)) == b);
  static_assert((c | semigroup_op(c)) == c);

  EXPECT_EQ((a | semigroup_op(b)), E{right{sum_int{7}}});
  EXPECT_EQ(((a | semigroup_op(b)) | semigroup_op(a)), E{right{sum_int{12}}});
  EXPECT_EQ((a | semigroup_op(b | semigroup_op(a))), E{right{sum_int{12}}});
  EXPECT_EQ((a | semigroup_op(c)), a);
  EXPECT_EQ((c | semigroup_op(b)), b);
  EXPECT_EQ((c | semigroup_op(c)), c);
}

TEST(Semigroup, EitherInplace) {
  using namespace prelude;
  using E = either<Error, sum_int>;

  E a{right{sum_int{5}}};
  const E b{right{sum_int{2}}};
  const E c{left{Error::ERROR_1}};

  a | semigroup_op(b);
  EXPECT_EQ(a, E{right{sum_int{7}}});

  a | semigroup_op(b);
  EXPECT_EQ(a, E{right{sum_int{9}}});

  a | semigroup_op(a);
  EXPECT_EQ(a, E{right{sum_int{18}}});

  a | semigroup_op(c) | semigroup_op(b);
  EXPECT_EQ(a, E{right{sum_int{20}}});
}
