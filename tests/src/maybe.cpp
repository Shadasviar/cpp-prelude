#include <prelude/functional.h>

#include <gtest/gtest.h>

template <typename T>
constexpr T f(int x) {
  return x / 2.0;
}

constexpr float g(float x) {
  return x + 1;
}

constexpr int mul2(int x) {
  return x * 2;
}

constexpr double sum(int a, float b, double c) {
  return a + b + c;
}

constexpr prelude::maybe<double> div_not_null(double x) {
  if (x == 0) return prelude::nothing;
  return prelude::just<double>(1 / x);
}

constexpr prelude::maybe<double> gen_num() {
  return prelude::just<double>(0.5);
}

make_func_object(sum_f, sum);
make_func_object(f_int, f<int>);

TEST(Maybe, Functor) {
  using namespace prelude;
  constexpr maybe<int> a{5};
  constexpr maybe<float> b;

  static_assert((a | map(f<float>)) == just<double>{2.5});
  static_assert((a | map(f<float>) | map(g)) == just<double>{3.5});
  static_assert((b | map(f<float>)) == nothing);
  static_assert((b | map(g)) == nothing);

  EXPECT_EQ((a | map(f<float>)), just<double>{2.5});
  EXPECT_EQ((a | map(f<float>) | map(g)), just<double>{3.5});
  EXPECT_EQ((b | map(f<float>)), nothing);
  EXPECT_EQ((b | map(g)), nothing);
}

TEST(Maybe, Applicative) {
  using namespace prelude;

  constexpr maybe<decltype(sum_f)> funOk{sum_f};
  constexpr maybe<decltype(sum_f)> funBad;
  constexpr maybe<int> a{5};
  constexpr maybe<float> b{1.5};
  constexpr maybe<double> c{7.5};
  constexpr maybe<int> bad = nothing;

  static_assert((funOk | lift(a, b, c)) == just<double>{14.0});
  static_assert((funBad | lift(a, b, c)) == nothing);
  static_assert((funOk | lift(a, bad, c)) == nothing);
  static_assert((funBad | lift(bad, b, c)) == nothing);

  EXPECT_EQ((funOk | lift(a, b, c)), just<double>{14});
  EXPECT_EQ((funBad | lift(a, b, c)), nothing);
  EXPECT_EQ((funOk | lift(a, bad, c)), nothing);
  EXPECT_EQ((funBad | lift(bad, b, c)), nothing);
}

TEST(Maybe, Monad) {
  using namespace prelude;

  constexpr maybe<double> a{2};
  constexpr maybe<double> b{0};
  constexpr maybe<double> bad = nothing;

  static_assert((a | prelude::bind(div_not_null)) == just<double>{0.5});
  static_assert((b | prelude::bind(div_not_null)) == nothing);
  static_assert((bad | prelude::bind(div_not_null)) == nothing);
  static_assert((a | void_bind(gen_num)) == just<double>{0.5});
  static_assert((a | void_bind(b)) == just<double>{0});
  static_assert((a | void_bind(bad)) == nothing);
  static_assert((bad | void_bind(a)) == nothing);

  EXPECT_EQ((a | prelude::bind(div_not_null)), just<double>{0.5});
  EXPECT_EQ((b | prelude::bind(div_not_null)), nothing);
  EXPECT_EQ((bad | prelude::bind(div_not_null)), nothing);
  EXPECT_EQ((a | void_bind(gen_num)), just<double>{0.5});
  EXPECT_EQ((a | void_bind(b)), just<double>{0});
  EXPECT_EQ((a | void_bind(bad)), nothing);
  EXPECT_EQ((bad | void_bind(a)), nothing);
}

TEST(Maybe, MonadInplace) {
  using namespace prelude;

  maybe<double> a{2};
  maybe<double> b{0};
  maybe<double> c{2};
  maybe<double> d{0};
  maybe<double> bad = nothing;

  a | prelude::bind(div_not_null);
  auto res = b | prelude::bind(div_not_null);
  bad | prelude::bind(div_not_null);
  c | void_bind(gen_num);
  d | void_bind(c);

  EXPECT_EQ(a, just<double>{0.5});
  EXPECT_EQ(b, nothing);
  EXPECT_EQ(c, just<double>(0.5));
  EXPECT_EQ(d, just<double>(0.5));
  EXPECT_EQ(bad, nothing);
  EXPECT_EQ(res, nothing);
}

TEST(Maybe, PerformanceFunctor) {
  using namespace prelude;

  maybe<int> c{5};
  for (int i = 0; i < 1e8; ++i) {
    c = cr(c) | map(mul2) | map(f<int>) | map(mul2) | map(f_int);
  }

  EXPECT_EQ(c, just<int>{5});
}

TEST(Maybe, PerformanceFunctorInplace) {
  using namespace prelude;

  maybe<int> c{5};
  for (int i = 0; i < 1e8; ++i) {
    c | map(mul2) | map(f<int>) | map(mul2) | map(f_int);
  }

  EXPECT_EQ(c, just<int>{5});
}

TEST(Maybe, PerformanceFunctorCompose) {
  using namespace prelude;

  maybe<int> c{5};
  for (int i = 0; i < 1e8; ++i) {
    c = cr(c) | map(mul2 * f_int * mul2 * f_int);
  }

  EXPECT_EQ(c, just<int>{5});
}

TEST(Maybe, PerformanceFunctorComposeInplace) {
  using namespace prelude;

  maybe<int> c{5};
  for (int i = 0; i < 1e8; ++i) {
    c | map(mul2 * f_int * mul2 * f_int);
  }

  EXPECT_EQ(c, just<int>{5});
}

TEST(Maybe, PerformanceMonad) {
  using namespace prelude;

  maybe<double> c{5};
  for (int i = 0; i < 1e8; ++i) {
    c = cr(c) | prelude::bind(div_not_null) | prelude::bind(div_not_null);
  }

  EXPECT_EQ(c, just<double>{5});
}

TEST(Maybe, PerformanceMonadInplace) {
  using namespace prelude;

  maybe<double> c{5};
  for (int i = 0; i < 1e8; ++i) {
    c | prelude::bind(div_not_null) | prelude::bind(div_not_null);
  }

  EXPECT_EQ(c, just<double>{5});
}
