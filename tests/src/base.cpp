#include <prelude/functional.h>

#include <gtest/gtest.h>

constexpr float f(int x) {
  return x / 2.0;
}

constexpr float g(float x) {
  return x + 1;
}

struct S {
  constexpr float div(float x, float z) const {
    return x / y + z;
  }

  float y = 2.0f;
};

make_func_object(G, g)
make_func_object(F, f)
make_func_object(Id, prelude::id<int>)

TEST(Base, Curry) {
  constexpr S s{5.0f};

  static_assert(curry(f, 2)() == 1);
  static_assert(curry(f)(2) == 1);
  static_assert(curry(f, 1)() == 0.5);
  static_assert(curry_class(s, div, 10)(2) == 4.0f);
  static_assert(curry_class(s, div)(10, 2) == 4.0f);
  static_assert(curry_class(s, div, 10, 2)() == 4.0f);

  EXPECT_EQ(curry(f, 2)(), 1);
  EXPECT_EQ(curry(f)(2), 1);
  EXPECT_EQ(curry(f, 1)(), 0.5);
  EXPECT_EQ(curry_class(s, div, 10)(2), 4.0f);
  EXPECT_EQ(curry_class(s, div)(10, 2), 4.0f);
  EXPECT_EQ(curry_class(s, div, 10, 2)(), 4.0f);
}

TEST(Base, CompositionFunction) {
  using namespace prelude;
  static_assert(compose(g, f)(5) == g(f(5)));
  EXPECT_EQ(compose(g,f)(5), g(f(5)));
}

TEST(Base, CompositionOperator) {
  using namespace prelude;
  static_assert((G * F)(5) == f(g(5)));
  static_assert(G(5) == 6);
  EXPECT_EQ((G * F)(5), f(g(5)));
  EXPECT_EQ(G(5), 6);
}

TEST(Base, Identity) {
  using namespace prelude;

  static_assert(id(5) == 5);
  static_assert((G * Id)(7) == 8);
  static_assert((Id * G)(7) == 8);

  EXPECT_EQ(id(5), 5);
  EXPECT_EQ((G * Id)(7), 8);
  EXPECT_EQ((Id * G)(7), 8);
}

TEST(Base, ApplyOperator) {
  using namespace prelude;

  static_assert(f(5) == (5 | F));
  static_assert(f(7) == (7 | F));
  static_assert(f(0) == (0 | F));
  static_assert(f(f(5)) == (5 | F | F));
  static_assert(f(g(25)) == (25 | G | F));

  EXPECT_EQ(f(5), (5 | F));
  EXPECT_EQ(f(7), (7 | F));
  EXPECT_EQ(f(0), (0 | F));
  EXPECT_EQ(f(f(5)), (5 | F | F));
  EXPECT_EQ(f(g(25)), (25 | G | F));
}
