#include <prelude/functional.h>

#include <string>
#include <numeric>

#include <gtest/gtest.h>
#include <gtest_helpers.h>

template <typename T>
struct Point {
  T x, y;

  constexpr bool operator==(const Point<T> &) const = default;
};

static constexpr int mul2(int x){return x*2;}
static constexpr double div2(double x){return x/2;}

make_func_object(div2_f, div2)
make_func_object(mul2_f, mul2)

static constexpr int mul(int x, int y){return x * y;}
static constexpr double fx(int x, int y, double z) {return (x + y) / z;}
static constexpr double fy(int x, int y, double z) {return (x * y) / z;}

template
  < typename ... Ts
  , typename T
  , typename Res = std::invoke_result_t<T, Ts...>
  >
constexpr Point<Res> lift_impl(const Point<T> &p, const Point<Ts>& ... pf) {
  return Point<Res>{.x = p.x((pf.x)...), .y = p.y((pf.y)...)};
}

TEST(Applicative, Point) {
  using namespace prelude;

  constexpr Point<int> a{.x = 2, .y = 3};
  constexpr Point<int> b{.x = 1, .y = 2};
  constexpr Point<double> c{.x = 1.5, .y = 2};
  constexpr Point<decltype(mul2)*> f1{.x = mul2, .y = mul2};
  constexpr Point<decltype(mul)*> f2{.x = mul, .y = mul};
  constexpr Point<decltype(fx)*> f3{.x = fx, .y = fy};

  constexpr auto d = f1 | lift(a);
  constexpr auto e = f2 | lift(a, b);
  constexpr auto g = f3 | lift(a, b, c);

  static_assert(d == Point<int>{.x = 4, .y = 6});
  static_assert(e == Point<int>{.x = 2, .y = 6});
  static_assert(g == Point<double>{.x = 2, .y = 3});

  EXPECT_EQ(d, (Point<int>{.x = 4, .y = 6}));
  EXPECT_EQ(e, (Point<int>{.x = 2, .y = 6}));
  EXPECT_EQ(g, (Point<double>{.x = 2, .y = 3}));
}

TEST(Applicative, Vector) {
  using namespace prelude;

  std::vector<int> a{1,2,3};
  std::vector<int> b{3,2,1};
  std::vector<double> c{5,4};
  std::vector<decltype(fx)*> f{fx, fy};
  std::vector<decltype(mul)*> f2{mul};

  const std::vector<double> expected_f {
    0.8,1.0,0.6,0.75,0.4,0.5,1.0,1.25,0.8,
    1.0,0.6,0.75,1.2,1.5,1.0,1.25,0.8,1.0,
    0.6,0.75,0.4,0.5,0.2,0.25,1.2,1.5,0.8,
    1.0,0.4,0.5,1.8,2.25,1.2,1.5,0.6,0.75,
  };

  const std::vector<int> expected_f2 {
    3,2,1,6,4,2,9,6,3,
  };

  auto f_res = f | lift(a, b, c);
  auto f2_res = f2 | lift(a, b);

  EXPECT_VECTORS_EQ(f_res, expected_f);
  EXPECT_VECTORS_EQ(f2_res, expected_f2);
}

TEST(Applicative, Array) {
  using namespace prelude;

  constexpr std::array<int, 3> a{1,2,3};
  constexpr std::array<int, 3> b{3,2,1};
  constexpr std::array<double, 2> c{5,4};
  constexpr std::array<decltype(fx)*, 2> f{fx, fy};
  constexpr std::array<decltype(mul)*, 1> f2{mul};

  constexpr std::array<double, 36> expected_f {
    0.8,1.0,0.6,0.75,0.4,0.5,1.0,1.25,0.8,
    1.0,0.6,0.75,1.2,1.5,1.0,1.25,0.8,1.0,
    0.6,0.75,0.4,0.5,0.2,0.25,1.2,1.5,0.8,
    1.0,0.4,0.5,1.8,2.25,1.2,1.5,0.6,0.75,
  };

  constexpr std::array<int,9> expected_f2 {
    3,2,1,6,4,2,9,6,3,
  };

  constexpr auto f_res = f | lift(a, b, c);
  constexpr auto f2_res = f2 | lift(a, b);

  static_assert(f_res == expected_f);
  static_assert(f2_res == expected_f2);

  EXPECT_VECTORS_EQ(f_res, expected_f);
  EXPECT_VECTORS_EQ(f2_res, expected_f2);
}

TEST(Applicative, VectorPerformance) {
  using namespace prelude;

  constexpr size_t size = 1e7;
  constexpr size_t n_iterations = 1e1;

  auto f = mul2_f * div2_f * div2_f *div2_f * mul2_f;
  std::vector<int> input(size);
  std::vector<int> expected(size);
  std::vector<decltype(f)> fa{f};

  std::iota(input.begin(), input.end(), 0);
  expected = cr(input) | map(f);

  std::vector<int> res;

  for (size_t i = 0; i < n_iterations; ++i) {
    res = fa | lift(input);
  }

  EXPECT_VECTORS_EQ(res, expected);
}
