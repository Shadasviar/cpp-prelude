#include <prelude/functional.h>

#include <string>

#include <gtest/gtest.h>

enum class Error {
  NOT_EVEN,
  NO_FUNCTION,
  MAPPED,
  ERROR,
  NO_ADD,
};

template <typename T>
constexpr T f(int x) {
  return x / 2.0;
}

constexpr float add(int x, int y) {
  return x + y;
}

constexpr float add3(int x, int y, float z) {
  return x + y + z;
}

constexpr prelude::either<Error, int> even_mul2_or_error(int x) {
  using namespace prelude;

  if (x % 2 == 0) return right{x * 2};
  return left{Error::NOT_EVEN};
}

constexpr prelude::either<Error, int> get5() {
  return prelude::right{5};
}

make_func_object(F, f<float>);
make_func_object(add_f, add);
make_func_object(add3_f, add3);

constexpr float g(float x){return x + 1;}
constexpr Error lf(Error) {return Error::MAPPED;}
constexpr bool get_r(float){return true;}
constexpr bool get_l(Error){return false;}

TEST(Either, API) {
  using namespace prelude;
  using either_float = either<Error, float>;

  constexpr either_float a{right{5.5f}};
  constexpr either_float b{left{Error::ERROR}};

  static_assert((a | curry(map_right, g)) == either_float{right{6.5f}});
  static_assert((a | curry(map_left, lf)) == either_float{right{5.5f}});
  static_assert((b | curry(map_right, g)) == either_float{left{Error::ERROR}});
  static_assert((b | curry(map_left, lf)) == either_float{left{Error::MAPPED}});
  static_assert((a | curry(either_visit, get_l, get_r)) == true);
  static_assert((b | curry(either_visit, get_l, get_r)) == false);

  EXPECT_EQ((a | curry(map_right, g)), either_float{right{6.5f}});
  EXPECT_EQ((a | curry(map_left, lf)), either_float{right{5.5f}});
  EXPECT_EQ((b | curry(map_right, g)), either_float{left{Error::ERROR}});
  EXPECT_EQ((b | curry(map_left, lf)), either_float{left{Error::MAPPED}});
  EXPECT_EQ((a | curry(either_visit, get_l, get_r)), true);
  EXPECT_EQ((b | curry(either_visit, get_l, get_r)), false);
}

TEST(Either, API_Inplace) {
  using namespace prelude;
  using either_float = either<Error, float>;

  either_float a{right{5.5f}};
  either_float b{left{Error::ERROR}};

  a | curry(map_right, g);
  EXPECT_EQ(a, either_float{right{6.5f}});

  a | curry(map_right, g) | curry(map_left, lf);
  EXPECT_EQ(a, either_float{right{7.5f}});

  b | curry(map_right, g);
  EXPECT_EQ(b, either_float{left{Error::ERROR}});

  b | curry(map_left, lf);
  EXPECT_EQ(b, either_float{left{Error::MAPPED}});
}

TEST(Either, Functor) {
  using namespace prelude;
  using either_float = either<Error, float>;

  constexpr either<Error, int> a{right{5}};
  constexpr either<Error, float> b{left{Error::ERROR}};

  static_assert((a | map(f<float>)) == either_float{right{2.5f}});
  static_assert((a | map(f<float>) | map(g)) == either_float{right{3.5f}});
  static_assert((b | map(f<float>)) == either_float{left{Error::ERROR}});
  static_assert((b | map(g)) == either_float{left{Error::ERROR}});

  EXPECT_EQ((a | map(f<float>)), either_float{right{2.5f}});
  EXPECT_EQ((a | map(f<float>) | map(g)), either_float{right{3.5f}});
  EXPECT_EQ((b | map(f<float>)), either_float{left{Error::ERROR}});
  EXPECT_EQ((b | map(g)), either_float{left{Error::ERROR}});
}

TEST(Either, FunctorInplace) {
  using namespace prelude;
  using either_float = either<Error, float>;

  either_float a{right{5.f}};
  either_float b{left{Error::ERROR}};

  auto x = a | map(g) | map(g);
  b | map(g);

  EXPECT_EQ(a, either_float{right{7.f}});
  EXPECT_EQ(x, a);
  EXPECT_EQ(b, either_float{left{Error::ERROR}});
}

TEST(Either, Applicative) {
  using namespace prelude;
  using either_float = either<Error, float>;

  constexpr const either<Error, decltype(F)> f1{right<decltype(F)>{F}};
  constexpr const either<Error, decltype(F)> f2{left{Error::NO_FUNCTION}};
  constexpr const either<Error, decltype(add_f)>
    f3{right<decltype(add_f)>{add_f}};
  constexpr const either<Error, decltype(add_f)> f4{left{Error::NO_ADD}};
  constexpr const either<Error, decltype(add3_f)>
    f5{right<decltype(add3_f)>{add3_f}};
  constexpr const either<Error, int> a{right{5}};
  constexpr const either_float b{left{Error::ERROR}};
  constexpr const either<Error, int> c{right{2}};
  constexpr const either_float d{right{1.5f}};

  static_assert((f1 | lift(a)) == either_float{right{2.5f}});
  static_assert((f2 | lift(a)) == either_float{left{Error::NO_FUNCTION}});
  static_assert((f1 | lift(b)) == either_float{left{Error::ERROR}});
  static_assert((f2 | lift(b)) == either_float{left{Error::NO_FUNCTION}});
  static_assert((f3 | lift(a, c)) == either_float{right{7.f}});
  static_assert((f4 | lift(a, c)) == either_float{left{Error::NO_ADD}});
  static_assert((f5 | lift(a, c, d)) == either_float{right{8.5f}});
  static_assert((f5 | lift(a, c, b)) == either_float{left{Error::ERROR}});

  EXPECT_EQ((f1 | lift(a)), either_float{right{2.5f}});
  EXPECT_EQ((f2 | lift(a)), either_float{left{Error::NO_FUNCTION}});
  EXPECT_EQ((f1 | lift(b)), either_float{left{Error::ERROR}});
  EXPECT_EQ((f2 | lift(b)), either_float{left{Error::NO_FUNCTION}});
  EXPECT_EQ((f3 | lift(a, c)), either_float{right{7.f}});
  EXPECT_EQ((f4 | lift(a, c)), either_float{left{Error::NO_ADD}});
  EXPECT_EQ((f5 | lift(a, c, d)), either_float{right{8.5f}});
  EXPECT_EQ((f5 | lift(a, c, b)), either_float{left{Error::ERROR}});
}

TEST(Either, Monad) {
  using namespace prelude;
  using either_int = either<Error, int>;

  constexpr const either_int a{right{5}};
  constexpr const either_int b{right{4}};
  constexpr const either_int c{left{Error::ERROR}};

  constexpr auto res1 = a | prelude::bind(even_mul2_or_error);
  constexpr auto res2 = b | prelude::bind(even_mul2_or_error);
  constexpr auto res3 = c | prelude::bind(even_mul2_or_error);
  constexpr auto res4
    = b
    | prelude::bind(even_mul2_or_error)
    | prelude::bind(even_mul2_or_error)
    ;
  constexpr auto res5 = a | void_bind(get5);
  constexpr auto res6 = b | void_bind(get5);
  constexpr auto res7 = c | void_bind(get5);
  constexpr auto res8 = mvoid_bind_impl(b, a);
  constexpr auto res9 = b | void_bind(c);

  static_assert(res1 == either_int{left{Error::NOT_EVEN}});
  static_assert(res2 == either_int{right{8}});
  static_assert(res3 == either_int{left{Error::ERROR}});
  static_assert(res4 == either_int{right{16}});
  static_assert(res5 == either_int{right{5}});
  static_assert(res6 == either_int{right{5}});
  static_assert(res7 == either_int{left{Error::ERROR}});
  static_assert(res8 == either_int{right{4}});
  static_assert(res9 == either_int{left{Error::ERROR}});

  EXPECT_EQ(res1, either_int{left{Error::NOT_EVEN}});
  EXPECT_EQ(res2, either_int{right{8}});
  EXPECT_EQ(res3, either_int{left{Error::ERROR}});
  EXPECT_EQ(res4, either_int{right{16}});
  EXPECT_EQ(res5, either_int{right{5}});
  EXPECT_EQ(res6, either_int{right{5}});
  EXPECT_EQ(res7, either_int{left{Error::ERROR}});
  EXPECT_EQ(res8, either_int{right{4}});
  EXPECT_EQ(res9, either_int{left{Error::ERROR}});
}

TEST(Either, MonadInplace) {
  using namespace prelude;
  using either_int = either<Error, int>;

  either_int a{right{5}};
  either_int b{right{4}};
  either_int c{left{Error::ERROR}};

  a | prelude::bind(even_mul2_or_error);
  b | prelude::bind(even_mul2_or_error);
  c | prelude::bind(even_mul2_or_error);

  EXPECT_EQ(a, either_int{left{Error::NOT_EVEN}});
  EXPECT_EQ(b, either_int{right{8}});
  EXPECT_EQ(c, either_int{left{Error::ERROR}});

  b | prelude::bind(even_mul2_or_error) | prelude::bind(even_mul2_or_error);
  EXPECT_EQ(b, either_int{right{32}});

  a | void_bind(get5);
  EXPECT_EQ(a, either_int{left{Error::NOT_EVEN}});

  b | void_bind(get5);
  EXPECT_EQ(b, either_int{right{5}});

  b | void_bind(a);
  EXPECT_EQ(b, either_int{left{Error::NOT_EVEN}});

  a = either_int{right{2}};
  b = either_int{right{3}};
  a | void_bind(b);
  EXPECT_EQ(a, either_int{right{3}});
}
