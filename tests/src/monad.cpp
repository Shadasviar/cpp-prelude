#include <prelude/functional.h>

#include <gtest/gtest.h>
#include <gtest_helpers.h>

template <typename T>
struct Point {
  T x, y;
  using value_t = T;

  template
    < typename F
    , typename Res = typename std::invoke_result_t<F, T>::value_t
    >
  constexpr friend Point<Res> mbind_impl(F &&f, const Point &p) {
    auto res_x = f(p.x);
    auto res_y = f(p.y);
    return Point<Res>{.x = res_x.x + res_y.x, .y = res_x.y + res_y.y};
  }

  template
    < typename F
    , typename T2 = typename std::invoke_result_t<F>
    >
  constexpr friend T2 mvoid_bind_impl(F &&f, const Point&) {
    return std::invoke(std::forward<F>(f));
  }

  template <typename T2>
  constexpr friend Point<T2> mvoid_bind_impl(const Point<T2> &x, const Point&) {
    return x;
  }

  constexpr bool operator==(const Point<T> &) const = default;

};

template <typename T>
static constexpr Point<T> generatePoint(T x){return Point<T>{.x = x, .y = x};}
static constexpr Point<double> gen(){return Point<double>{.x = 1.5, .y = 2.5};}
static const std::vector<int> mul2(int x) {
  return std::vector<int>{x * 2, x};
}

make_func_object(generatePoint_f, generatePoint<int>)
make_func_object(generatePointd_f, generatePoint<double>)
make_func_object(gen_f, gen)

TEST(Monad, Point) {
  using namespace prelude;

  constexpr auto a = generatePoint_f(5);
  constexpr auto b = generatePoint_f(2);
  constexpr Point<double> c{.x = 1.5, .y = 2};

  constexpr auto d = a | bind(generatePoint_f);
  constexpr auto e
    = b
    | bind(generatePoint_f)
    | bind(generatePoint_f)
    | bind(generatePoint_f)
    ;
  constexpr auto g = a | void_bind(b) | void_bind(c);
  constexpr auto h = c | bind(generatePoint_f) | bind(generatePointd_f);
  constexpr auto i = a | bind(generatePoint_f) | void_bind(gen_f);

  static_assert(d == Point<int>{.x = 10, .y = 10});
  static_assert(e == Point<int>{.x = 16, .y = 16});
  static_assert(g == Point<double>{.x = 1.5, .y = 2});
  static_assert(h == Point<double>{.x = 6, .y = 6});
  static_assert(i == Point<double>{.x = 1.5, .y = 2.5});

  EXPECT_EQ(d, (Point<int>{.x = 10, .y = 10}));
  EXPECT_EQ(e, (Point<int>{.x = 16, .y = 16}));
  EXPECT_EQ(g, (Point<double>{.x = 1.5, .y = 2}));
  EXPECT_EQ(h, (Point<double>{.x = 6, .y = 6}));
  EXPECT_EQ(i, (Point<double>{.x = 1.5, .y = 2.5}));
}

TEST(Monad, Vector) {
  using namespace prelude;

  const std::vector<int> a{1,2,3};
  const std::vector<int> res1_expected{2,1,4,2,6,3};
  const std::vector<int> res2_expected{4,2,2,1,8,4,4,2,12,6,6,3};
  const std::vector<int> res4_expected{2,1};

  auto f = std::bind(mul2, 1);

  auto res1 = a | prelude::bind(mul2);
  auto res2 = a | prelude::bind(mul2) | prelude::bind(mul2);
  auto res3 = a | void_bind(res2) | void_bind(res1);
  auto res4 = a | void_bind(f);
  auto res5 = a | prelude::bind(mul2) | void_bind(f);

  EXPECT_VECTORS_EQ(res1, res1_expected);
  EXPECT_VECTORS_EQ(res2, res2_expected);
  EXPECT_VECTORS_EQ(res3, res1_expected);
  EXPECT_VECTORS_EQ(res4, res4_expected);
  EXPECT_VECTORS_EQ(res5, res4_expected);
}

TEST(Monad, InplaceVector) {
  using namespace prelude;

  std::vector<int> a{1,2,3};
  std::vector<int> b{1,2,3};
  std::vector<int> c{1,2,3};
  std::vector<int> d{1,2,3};

  auto f = std::bind(mul2, 1);

  const std::vector<int> res1_expected{2,1,4,2,6,3};
  const std::vector<int> res2_expected{4,2,2,1,8,4,4,2,12,6,6,3};
  const std::vector<int> res3_expected{2,1};

  auto res1 = a | prelude::bind(mul2);
  auto res2 = b | prelude::bind(mul2) | prelude::bind(mul2);
  auto res3 = c | void_bind(f);
  auto res4 = d | prelude::bind(mul2) | void_bind(a);

  EXPECT_VECTORS_EQ(res1, res1_expected);
  EXPECT_VECTORS_EQ(res2, res2_expected);
  EXPECT_VECTORS_EQ(res3, res3_expected);
  EXPECT_VECTORS_EQ(res4, res1_expected);
  EXPECT_VECTORS_EQ(a, res1_expected);
  EXPECT_VECTORS_EQ(b, res2_expected);
  EXPECT_VECTORS_EQ(c, res3_expected);
  EXPECT_VECTORS_EQ(a, res4);
}
