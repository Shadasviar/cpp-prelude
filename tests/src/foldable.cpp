#include <prelude/functional.h>

#include <array>
#include <vector>
#include <list>

#include <gtest/gtest.h>
#include <gtest_helpers.h>

enum class Error {
  ERROR,
};

template <typename T>
struct Point {
  T x, y;

  template
    < typename F
    , typename Res
    > requires std::same_as<std::invoke_result_t<F, Res, T>, std::decay_t<Res>>
  friend constexpr std::decay_t<Res> fold_impl
    ( F &&f
    , Res &&acc
    , const Point<T> &p
    ) {
    return f(f(std::forward<Res>(acc), p.x), p.y);
  }

  constexpr bool operator==(const Point<T>&) const = default;
  constexpr auto operator<=>(const Point<T>&) const = default;
};

constexpr double sum(double acc, int x) {
  return acc + x;
}

constexpr prelude::maybe<double> div_x(double acc, int x) {
  using namespace prelude;

  if (x == 0) return nothing;
  return just<double>{acc / x};
}

TEST(Foldable, Point) {
  using namespace prelude;

  constexpr Point<int> a{1,2};
  constexpr Point<int> b{3,7};

  static_assert((a | fold(sum, 0.5)) == 3.5);
  static_assert((a | fold(sum, 0.0)) == 3.0);
  static_assert((b | fold(sum, 0.0)) == 10.0);
  static_assert((b | fold(sum, 2.0)) == 12.0);

  EXPECT_EQ((a | fold(sum, 0.5)), 3.5);
  EXPECT_EQ((a | fold(sum, 0.0)), 3.0);
  EXPECT_EQ((b | fold(sum, 0.0)), 10.0);
  EXPECT_EQ((b | fold(sum, 2.0)), 12.0);
}

TEST(Foldable, Array) {
  using namespace prelude;

  constexpr std::array<int, 5> a{1,2,3,4,5};
  constexpr std::array<int, 3> b{2,4,6};

  static_assert((a | fold(sum, 0.0)) == 15.0);
  static_assert((a | fold(sum, 0.5)) == 15.5);
  static_assert((a | fold(sum, 2.6)) == 17.6);
  static_assert((b | fold(sum, 1.0)) == 13.0);
  static_assert((b | fold(sum, 7.0)) == 19.0);

  EXPECT_EQ((a | fold(sum, 0.0)), 15.0);
  EXPECT_EQ((a | fold(sum, 0.5)), 15.5);
  EXPECT_EQ((a | fold(sum, 2.6)), 17.6);
  EXPECT_EQ((b | fold(sum, 1.0)), 13.0);
  EXPECT_EQ((b | fold(sum, 7.0)), 19.0);
}

TEST(Foldable, Vector) {
  using namespace prelude;

  const std::vector<int> a{1,2,3,4,5};
  const std::vector<int> b{2,4,6};

  EXPECT_EQ((a | fold(sum, 0.0)), 15.0);
  EXPECT_EQ((a | fold(sum, 0.5)), 15.5);
  EXPECT_EQ((a | fold(sum, 2.6)), 17.6);
  EXPECT_EQ((b | fold(sum, 1.0)), 13.0);
  EXPECT_EQ((b | fold(sum, 7.0)), 19.0);
}

TEST(Foldable, CArray) {
  using namespace prelude;

  constexpr int a[] {1,2,3,4,5};
  constexpr int b[] {2,4,6};

  static_assert((a | fold(sum, 0.0)) == 15.0);
  static_assert((a | fold(sum, 0.5)) == 15.5);
  static_assert((a | fold(sum, 2.6)) == 17.6);
  static_assert((b | fold(sum, 1.0)) == 13.0);
  static_assert((b | fold(sum, 7.0)) == 19.0);

  EXPECT_EQ((a | fold(sum, 0.0)), 15.0);
  EXPECT_EQ((a | fold(sum, 0.5)), 15.5);
  EXPECT_EQ((a | fold(sum, 2.6)), 17.6);
  EXPECT_EQ((b | fold(sum, 1.0)), 13.0);
  EXPECT_EQ((b | fold(sum, 7.0)), 19.0);
}

TEST(Foldable, FoldM) {
  using namespace prelude;

  std::array<int, 3> a{1,2,2};
  std::array<int, 4> b{1,0,3,4};

  EXPECT_EQ((a | fold_m(div_x, 1.0)), just<double>{0.25});
  EXPECT_EQ((b | fold_m(div_x, 1.0)), nothing);
}

TEST(Foldable, FoldMaybes) {
  using namespace prelude;

  constexpr std::array<maybe<int>, 3> a{just<int>{1}, just<int>{2}, just<int>{2}};
  constexpr std::array<maybe<int>, 4> b{just<int>{1}, nothing, just<int>{3}, just<int>{4}};

  static_assert((a | fold(sum, 1.0)) == just<double>{6.0});
  static_assert((a | fold(sum, 2.5)) == just<double>{7.5});
  static_assert((b | fold(sum, 1.0)) == nothing);
  static_assert((b | fold(sum, 2.0)) == nothing);

  EXPECT_EQ((a | fold(sum, 1.0)), just<double>{6.0});
  EXPECT_EQ((a | fold(sum, 2.5)), just<double>{7.5});
  EXPECT_EQ((b | fold(sum, 1.0)), nothing);
  EXPECT_EQ((b | fold(sum, 2.0)), nothing);
}

TEST(Foldable, FoldEithers) {
  using namespace prelude;
  using either_int = either<Error, int>;
  using either_double = either<Error, double>;

  constexpr std::array<either_int, 3> a{right{1}, right{2}, right{2}};
  constexpr std::array<either_int, 4> b
    {right{1}, left{Error::ERROR}, right{3}, right{4}};
  std::vector<either_int> c{right{1}, right{2}, right{2}};
  std::list<either_int> d{right{1}, right{2}, right{2}};

  static_assert((a | fold(sum, 1.0)) == either_double{right{6.0}});
  static_assert((a | fold(sum, 2.5)) == either_double{right{7.5}});
  static_assert((b | fold(sum, 1.0)) == either_double{left{Error::ERROR}});
  static_assert((b | fold(sum, 3.0)) == either_double{left{Error::ERROR}});

  EXPECT_EQ((a | fold(sum, 1.0)), either_double{right{6.0}});
  EXPECT_EQ((a | fold(sum, 2.5)), either_double{right{7.5}});
  EXPECT_EQ((b | fold(sum, 1.0)), either_double{left{Error::ERROR}});
  EXPECT_EQ((b | fold(sum, 3.0)), either_double{left{Error::ERROR}});
  EXPECT_EQ((c | fold(sum, 1.0)), either_double{right{6.0}});
  EXPECT_EQ((d | fold(sum, 1.0)), either_double{right{6.0}});
}
