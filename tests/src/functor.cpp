#include <prelude/functional.h>

#include <string>
#include <cassert>
#include <numeric>
#include <algorithm>
#include <ranges>

#include <gtest/gtest.h>
#include <gtest_helpers.h>

template <typename T>
struct Point {
  T x, y;

  template
    < typename F
    , typename T2 = std::invoke_result_t<F, T>
    >
  friend constexpr Point<T2> fmap_impl(F &&f, const Point<T> &p) {
    return Point<T2>{.x = f(p.x), .y = f(p.y)};
  }

  constexpr bool operator==(const Point<T>&) const = default;
  constexpr auto operator<=>(const Point<T>&) const = default;
};

static constexpr int mul2(int x) {return x*2;}
static constexpr double div2(double x) {return x / 2;}
static std::string show(int x) {return std::to_string(x);}

make_func_object(div2_f, div2)
make_func_object(mul2_f, mul2)

TEST(Functor, Point) {
  using namespace prelude;

  constexpr auto a = Point<double> {.x = 5, .y = 4};
  constexpr auto b = a | map(div2) | map(div2) | map(mul2);
  constexpr auto c = Point<double>{.x = 2.5, .y = 2.5};
  constexpr auto d = a | map(div2_f * div2_f * mul2_f);
  static_assert(b == Point<int>{.x = 2, .y = 2});
  static_assert(c == Point<double>{.x = 2.5, .y = 2.5});
  static_assert(d == b);

  EXPECT_EQ(b, (Point<int>{.x = 2, .y = 2}));
  EXPECT_EQ(c, (Point<double>{.x = 2.5, .y = 2.5}));
  EXPECT_EQ(d, b);
}

TEST(Functor, Array) {
  using namespace prelude;

  constexpr std::array<int, 5> ca {1,2,3,4,5};
  constexpr auto cb = ca | map(div2) | map(div2) | map(mul2);
  static_assert(cb == std::array<int, 5> {0,0,0,2,2});

  constexpr auto cc = ca | map(div2_f * div2_f * mul2_f);
  static_assert(cc == cb);

  EXPECT_VECTORS_EQ(cb, (std::array<int, 5> {0,0,0,2,2}));
  EXPECT_VECTORS_EQ(cc, cb);
}

TEST(Functor, InplaceArray) {
  using namespace prelude;

  std::array<int, 5> ica {1,2,3,4,5};
  auto icb = ica | map(div2) | map(div2) | map(mul2);
  EXPECT_VECTORS_EQ(icb, ica);
  EXPECT_VECTORS_EQ(icb, (std::array<int, 5> {0,0,0,2,2}));
}

TEST(Functor, Vector) {
  using namespace prelude;

  const std::vector<int> vf {1,2,3,4,5};
  auto vf2 = vf | map(div2) | map(div2) | map(mul2);
  EXPECT_VECTORS_EQ(vf2, (std::vector<int>{0,0,0,2,2}));

  auto vf3 = vf | map(div2) | map(div2) | map(mul2) | map(show);
  EXPECT_VECTORS_EQ(vf3, (std::vector<std::string>{"0","0","0","2","2"}));
}

TEST(Functor, InplaceVector) {
  using namespace prelude;

  std::vector<int> ivf {1,2,3,4,5};
  auto ivf2 = ivf | map(mul2) | map(mul2);
  EXPECT_VECTORS_EQ(ivf2, ivf);
  EXPECT_VECTORS_EQ(ivf2, (std::vector<int>{4,8,12,16,20}));
}

/*************************** Performance tests ******************************/

class FunctorPerformance : public testing::Test {
  public:

    FunctorPerformance() {
      _input.resize(_size);
      _input_inplace.resize(_size);
      std::iota(_input.begin(), _input.end(), 0);
      std::iota(_input_inplace.begin(), _input_inplace.end(), 0);

      using namespace prelude;

      _expected = _input;
      for (size_t i = 0; i < _n_iterations; ++i) {
        _expected | map(mul2_f * mul2_f * div2_f * div2_f * mul2_f);
      }
    }

    void SetUp() {
      std::iota(_input_inplace.begin(), _input_inplace.end(), 0);
    }

    static constexpr size_t _size = 1e4;
    static constexpr size_t _n_iterations = 1e5;
    std::vector<int> _input;
    std::vector<int> _input_inplace;
    std::vector<int> _expected;
};

/* As for now, clang does not support std::views */
#ifndef __clang__
TEST_F(FunctorPerformance, StdRanges) {
  using namespace std::views;

  constexpr auto mul2_f = transform(mul2);
  constexpr auto div2_f = transform(div2);

  std::vector<int> res = _input;
  for (size_t i = 0; i < _n_iterations; ++i) {
    auto res_common_view = res | mul2_f | mul2_f | div2_f | div2_f | mul2_f | common;
    res = std::vector<int>(res_common_view.begin(), res_common_view.end());
  }

  EXPECT_VECTORS_EQ(res, _expected);
}
#endif

TEST_F(FunctorPerformance, Fmap) {
  using namespace prelude;

  std::vector<int> res = _input;
  for (size_t i = 0; i < _n_iterations; ++i) {
    res = cr(res) | map(mul2) | map(mul2) | map(div2) | map(div2) | map(mul2);
  }

  EXPECT_VECTORS_EQ(res, _expected);
}

TEST_F(FunctorPerformance, FmapComposed) {
  using namespace prelude;

  std::vector<int> res = _input;
  for (size_t i = 0; i < _n_iterations; ++i) {
    res = cr(res) | map(mul2_f * mul2_f * div2_f * div2_f * mul2_f);
  }

  EXPECT_VECTORS_EQ(res, _expected);
}

TEST_F(FunctorPerformance, FmapInplace) {
  using namespace prelude;

  for (size_t i = 0; i < _n_iterations; ++i) {
    _input_inplace | map(mul2) | map(mul2) | map(div2) | map(div2) | map(mul2);
  }

  EXPECT_VECTORS_EQ(_input_inplace, _expected);
}

TEST_F(FunctorPerformance, FmapComposedInplace) {
  using namespace prelude;

  for (size_t i = 0; i < _n_iterations; ++i) {
    _input_inplace | map(mul2_f * mul2_f * div2_f * div2_f * mul2_f);
  }

  EXPECT_VECTORS_EQ(_input_inplace, _expected);
}
