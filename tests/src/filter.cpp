#include <prelude/functional.h>

#include <string>
#include <vector>
#include <numeric>

#include <gtest/gtest.h>
#include <gtest_helpers.h>

struct int_add {
  int value;
  constexpr bool operator==(const int_add&) const = default;
};

static constexpr int_add semigroup_op_impl(int_add a, int_add b) {
  return {a.value + b.value};
}

static constexpr bool is_odd(int x) {return x % 2 != 0;}
static constexpr bool is_odd_add(int_add x) {return is_odd(x.value);}

TEST(Filter, Vector) {
  using namespace prelude;

  std::vector<int> a{1,2,3,4,5};
  std::vector<int> b{1,2,5,7,9,6};

  const std::vector<int> odds_a {1,3,5};
  const std::vector<int> odds_b {1,5,7,9};

  EXPECT_VECTORS_EQ((a | curry(filter, is_odd)), odds_a);
  EXPECT_VECTORS_EQ((b | curry(filter, is_odd)), odds_b);
}

TEST(Filter, Maybe) {
  using namespace prelude;

  constexpr maybe<int_add> a{int_add{5}};
  constexpr maybe<int_add> b;
  constexpr maybe<int_add> c{int_add{2}};

  static_assert((filter(is_odd_add, a)) == just<int_add>{int_add{5}});
  static_assert((filter(is_odd_add, b)) == nothing);
  static_assert((filter(is_odd_add, c)) == nothing);

  EXPECT_EQ((filter(is_odd_add, a)), just<int_add>{int_add{5}});
  EXPECT_EQ((filter(is_odd_add, b)), nothing);
  EXPECT_EQ((filter(is_odd_add, c)), nothing);
}
