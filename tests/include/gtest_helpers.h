#ifndef GTEST_HELPERS_H
#define GTEST_HELPERS_H

#define EXPECT_VECTORS_EQ(a, b) \
  ASSERT_EQ((a).size(), (b).size()); \
  for (size_t i = 0; i < (a).size(); ++i) { \
    EXPECT_EQ((a)[i], (b)[i]); \
  }

#endif /* GTEST_HELPERS_H */
